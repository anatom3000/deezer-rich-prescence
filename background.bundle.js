'use strict';
!function(modules) {
  /**
   * @param {number} moduleId
   * @return {?}
   */
  function __webpack_require__(moduleId) {
    if (installedModules[moduleId]) {
      return installedModules[moduleId].exports;
    }
    var module = installedModules[moduleId] = {
      i : moduleId,
      l : false,
      exports : {}
    };
    return modules[moduleId].call(module.exports, module, module.exports, __webpack_require__), module.l = true, module.exports;
  }
  var installedModules = {};
  /** @type {!Array} */
  __webpack_require__.m = modules;
  __webpack_require__.c = installedModules;
  /**
   * @param {!Function} exports
   * @param {string} name
   * @param {!Function} n
   * @return {undefined}
   */
  __webpack_require__.d = function(exports, name, n) {
    if (!__webpack_require__.o(exports, name)) {
      Object.defineProperty(exports, name, {
        configurable : false,
        enumerable : true,
        get : n
      });
    }
  };
  /**
   * @param {!Object} module
   * @return {?}
   */
  __webpack_require__.n = function(module) {
    /** @type {function(): ?} */
    var n = module && module.__esModule ? function() {
      return module.default;
    } : function() {
      return module;
    };
    return __webpack_require__.d(n, "a", n), n;
  };
  /**
   * @param {!Function} object
   * @param {string} name
   * @return {?}
   */
  __webpack_require__.o = function(object, name) {
    return Object.prototype.hasOwnProperty.call(object, name);
  };
  /** @type {string} */
  __webpack_require__.p = "";
  __webpack_require__(__webpack_require__.s = 1);
}([function(canCreateDiscussions, exports, n) {
  /**
   * @param {!AudioNode} that
   * @param {!Function} size
   * @return {undefined}
   */
  function get(that, size) {
    if (!(that instanceof size)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  Object.defineProperty(exports, "__esModule", {
    value : true
  });
  var installNativeEvent$2 = function() {
    /**
     * @param {!Function} d
     * @param {string} props
     * @return {undefined}
     */
    function t(d, props) {
      /** @type {number} */
      var i = 0;
      for (; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        /** @type {boolean} */
        descriptor.configurable = true;
        if ("value" in descriptor) {
          /** @type {boolean} */
          descriptor.writable = true;
        }
        Object.defineProperty(d, descriptor.key, descriptor);
      }
    }
    return function(p, n, a) {
      return n && t(p.prototype, n), a && t(p, a), p;
    };
  }();
  var Constant = function() {
    /**
     * @return {undefined}
     */
    function n() {
      get(this, n);
      /** @type {string} */
      this._tokensPolicy = "last";
      /** @type {null} */
      this._lastTokenFound = null;
      /** @type {!Array} */
      this._tokensFound = [];
      /** @type {!Array} */
      this._tokensSelection = [];
      /** @type {boolean} */
      this._notificationsEnabled = true;
      /** @type {boolean} */
      this._loggingEnabled = true;
    }
    return installNativeEvent$2(n, [{
      key : "load",
      value : function() {
        var obj = this;
        return new Promise(function(saveNotifs) {
          /** @type {!Array<?>} */
          var nodeIds = Object.keys(obj).map(function(OldString) {
            return OldString.substring(1);
          });
          chrome.storage.sync.get(nodeIds, function(options) {
            Object.assign(obj, options);
            var changed = {};
            /** @type {boolean} */
            var _iteratorNormalCompletion3 = true;
            /** @type {boolean} */
            var s = false;
            var r = void 0;
            try {
              var _step3;
              var _iterator3 = nodeIds[Symbol.iterator]();
              for (; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                var name = _step3.value;
                changed[name] = {
                  oldValue : obj["_" + name],
                  newValue : options[name] ? options[name] : obj["_" + name]
                };
              }
            } catch (G__20648) {
              /** @type {boolean} */
              s = true;
              r = G__20648;
            } finally {
              try {
                if (!_iteratorNormalCompletion3 && _iterator3.return) {
                  _iterator3.return();
                }
              } finally {
                if (s) {
                  throw r;
                }
              }
            }
            if (obj._onChangedCallback) {
              obj._onChangedCallback(changed);
            }
            if (obj.loggingEnabled) {
              console.log("Config set to: " + JSON.stringify(obj, null, 4));
            }
            chrome.storage.onChanged.addListener(function(params, n) {
              /** @type {boolean} */
              var isProperty = false;
              var i;
              for (i in params) {
                if (obj["_" + i] !== params[i].newValue) {
                  obj["_" + i] = params[i].newValue;
                  /** @type {boolean} */
                  isProperty = true;
                  if (obj._loggingEnabled || "loggingEnabled" === i) {
                    console.log("Config '" + i + "' changed to " + params[i].newValue + ".");
                  }
                }
              }
              if (isProperty && obj._onChangedCallback) {
                obj._onChangedCallback(params);
              }
            });
            saveNotifs();
          });
        });
      }
    }, {
      key : "notificationsEnabled",
      get : function() {
        return this._notificationsEnabled;
      },
      set : function(data) {
        if (this._notificationsEnabled !== data) {
          var artistTrack = {
            notificationsEnabled : {
              oldValue : this._notificationsEnabled,
              newValue : data
            }
          };
          /** @type {!Object} */
          this._notificationsEnabled = data;
          chrome.storage.sync.set({
            notificationsEnabled : data
          });
          if (this._onChangedCallback) {
            this._onChangedCallback(artistTrack);
          }
        }
      }
    }, {
      key : "lastTokenFound",
      get : function() {
        return this._lastTokenFound;
      },
      set : function(data) {
        if (this._lastTokenFound !== data) {
          var artistTrack = {
            lastTokenFound : {
              oldValue : this._lastTokenFound,
              newValue : data
            }
          };
          /** @type {!Object} */
          this._lastTokenFound = data;
          chrome.storage.sync.set({
            lastTokenFound : data
          });
          if (this._onChangedCallback) {
            this._onChangedCallback(artistTrack);
          }
        }
      }
    }, {
      key : "tokensPolicy",
      get : function() {
        return this._tokensPolicy;
      },
      set : function(data) {
        if (this._tokensPolicy !== data) {
          var artistTrack = {
            tokensPolicy : {
              oldValue : this._tokensPolicy,
              newValue : data
            }
          };
          /** @type {!Object} */
          this._tokensPolicy = data;
          chrome.storage.sync.set({
            tokensPolicy : data
          });
          if (this._onChangedCallback) {
            this._onChangedCallback(artistTrack);
          }
        }
      }
    }, {
      key : "tokensFound",
      get : function() {
        return this._tokensFound;
      },
      set : function(data) {
        if (this._tokensFound !== data) {
          var artistTrack = {
            tokensFound : {
              oldValue : this._tokensFound,
              newValue : data
            }
          };
          /** @type {!Object} */
          this._tokensFound = data;
          chrome.storage.sync.set({
            tokensFound : data
          });
          if (this._onChangedCallback) {
            this._onChangedCallback(artistTrack);
          }
        }
      }
    }, {
      key : "tokensSelection",
      get : function() {
        return this._tokensSelection;
      },
      set : function(data) {
        if (this._tokensSelection !== data) {
          var artistTrack = {
            tokensSelection : {
              oldValue : this._tokensSelection,
              newValue : data
            }
          };
          /** @type {!Object} */
          this._tokensSelection = data;
          chrome.storage.sync.set({
            tokensSelection : data
          });
          if (this._onChangedCallback) {
            this._onChangedCallback(artistTrack);
          }
        }
      }
    }, {
      key : "loggingEnabled",
      get : function() {
        return this._loggingEnabled;
      },
      set : function(data) {
        if (this._loggingEnabled !== data) {
          var actual = {
            loggingEnabled : {
              oldValue : this._loggingEnabled,
              newValue : data
            }
          };
          /** @type {!Object} */
          this._loggingEnabled = data;
          chrome.storage.sync.set({
            loggingEnabled : data
          });
          if (this._onChangedCallback) {
            this._onChangedCallback(actual);
          }
        }
      }
    }, {
      key : "onChanged",
      set : function(e) {
        /** @type {boolean} */
        this._onChangedCallback = e;
      }
    }]), n;
  }();
  exports.default = new Constant;
}, function(canCreateDiscussions, isSlidingUp, saveNotifs) {
  /**
   * @param {!Object} obj
   * @return {?}
   */
  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default : obj
    };
  }
  /**
   * @param {string} word
   * @return {?}
   */
  function t(word) {
    var b = word.split(a);
    return 3 === b.length ? b[1] + a + b[0] + a + b[2] : word.replace(f, "");
  }
  /**
   * @param {!Array} e
   * @return {undefined}
   */
  function update(e) {
    var i;
    for (i in messages) {
      if (!e.includes(i)) {
        messages[i].sendStatusUpdate(null);
        messages[i].close();
        delete messages[i];
        if (config.default.loggingEnabled) {
          console.log("Removed gateway for token " + i);
        }
      }
    }
    e.forEach(function(name) {
      if (!messages[name]) {
        var m = new _noframeworkWaypoints2.default(name);
        messages[name] = m;
        var n = tween1.currentAudibleTab;
        var y = n ? t(n.title) : null;
        m.sendStatusUpdate(y);
        if (config.default.loggingEnabled) {
          console.log("Added gateway for token " + name);
        }
      }
    });
  }
  var config = _interopRequireDefault(saveNotifs(0));
  var _custom2 = _interopRequireDefault(saveNotifs(2));
  var _UiIcon2 = _interopRequireDefault(saveNotifs(3));
  var _noframeworkWaypoints2 = _interopRequireDefault(saveNotifs(4));
  var _readArchive2 = _interopRequireDefault(saveNotifs(5));
  /** @type {string} */
  var a = " - ";
  var messages = {};
  /** @type {!RegExp} */
  var f = new RegExp(a + "Deezer(?=" + a + ")", "g");
  var tween1 = new _UiIcon2.default("www.deezer.com", 3E3);
  /**
   * @param {!Object} d
   * @return {undefined}
   */
  tween1.onCurrentAudibleTabChange = function(d) {
    /** @type {!Array<?>} */
    var ret = Object.values(messages);
    if (ret.length > 0) {
      var vx = d ? t(d.title) : null;
      /** @type {boolean} */
      var _iteratorNormalCompletion3 = true;
      /** @type {boolean} */
      var o = false;
      var l = void 0;
      try {
        var $__6;
        var _iterator3 = ret[Symbol.iterator]();
        for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
          $__6.value.sendStatusUpdate(vx);
        }
      } catch (lightThemeBackground) {
        /** @type {boolean} */
        o = true;
        l = lightThemeBackground;
      } finally {
        try {
          if (!_iteratorNormalCompletion3 && _iterator3.return) {
            _iterator3.return();
          }
        } finally {
          if (o) {
            throw l;
          }
        }
      }
      if (config.default.notificationsEnabled) {
        _custom2.default.notify(chrome.i18n.getMessage("status") + ": " + (vx || chrome.i18n.getMessage("statusNone")), d ? function() {
          chrome.tabs.update(d.id, {
            active : true
          });
          if (config.default.loggingEnabled) {
            console.log("Status notification '" + vx + "' clicked, tab switched to " + d.id + ".");
          }
        } : null);
      }
    } else {
      if (config.default.loggingEnabled) {
        console.log("Status changed but no Discord token set.");
      }
    }
  };
  var params = new _readArchive2.default;
  /**
   * @param {!Object} callback
   * @return {undefined}
   */
  params.onFound = function(callback) {
    if (config.default.lastTokenFound !== callback && (config.default.lastTokenFound = callback), !config.default.tokensFound.includes(callback)) {
      var fixedRightWatchers = config.default.tokensFound.slice();
      fixedRightWatchers.push(callback);
      config.default.tokensFound = fixedRightWatchers;
    }
  };
  /**
   * @param {?} value
   * @return {undefined}
   */
  config.default.onChanged = function(value) {
    switch(console.log("changeInfo: " + JSON.stringify(value, null, 4)), config.default.tokensPolicy) {
      case "last":
        update(config.default.lastTokenFound ? [config.default.lastTokenFound] : []);
        break;
      case "all":
        update(config.default.tokensFound);
        break;
      case "selection":
        update(config.default.tokensSelection);
    }
  };
  config.default.load().then(function() {
    params.start();
    tween1.start();
  });
}, function(canCreateDiscussions, option, saveNotifs) {
  /**
   * @param {!AudioNode} that
   * @param {!Function} size
   * @return {undefined}
   */
  function get(that, size) {
    if (!(that instanceof size)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  Object.defineProperty(option, "__esModule", {
    value : true
  });
  var installNativeEvent$2 = function() {
    /**
     * @param {!Function} d
     * @param {string} props
     * @return {undefined}
     */
    function t(d, props) {
      /** @type {number} */
      var i = 0;
      for (; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        /** @type {boolean} */
        descriptor.configurable = true;
        if ("value" in descriptor) {
          /** @type {boolean} */
          descriptor.writable = true;
        }
        Object.defineProperty(d, descriptor.key, descriptor);
      }
    }
    return function(p, n, a) {
      return n && t(p.prototype, n), a && t(p, a), p;
    };
  }();
  var o = function(obj) {
    return obj && obj.__esModule ? obj : {
      default : obj
    };
  }(saveNotifs(0));
  var browser = chrome.i18n.getMessage("appName");
  var Buffer = function() {
    /**
     * @return {undefined}
     */
    function n() {
      get(this, n);
    }
    return installNativeEvent$2(n, [{
      key : "notify",
      value : function(error, value) {
        if (o.default.notificationsEnabled) {
          /** @type {boolean} */
          var cb = "function" == typeof value;
          chrome.notifications.create({
            type : "basic",
            iconUrl : "discord48.png",
            title : browser,
            message : error,
            isClickable : cb
          }, function(directories) {
            if (cb) {
              /**
               * @param {?} key
               * @return {undefined}
               */
              var listener = function(key) {
                if (key === directories) {
                  value();
                }
              };
              chrome.notifications.onClicked.addListener(listener);
              chrome.notifications.onClosed.removeListener(listener);
            }
          });
          if (o.default.loggingEnabled) {
            console.log("New notification '" + error + "'");
          }
        }
      }
    }]), n;
  }();
  option.default = new Buffer;
}, function(canCreateDiscussions, t, saveNotifs) {
  /**
   * @param {!AudioNode} element
   * @param {!Function} parent
   * @return {undefined}
   */
  function fn(element, parent) {
    if (!(element instanceof parent)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  Object.defineProperty(t, "__esModule", {
    value : true
  });
  /** @type {function(!Object, ...(Object|null)): !Object} */
  var $ = Object.assign || function(target) {
    /** @type {number} */
    var i = 1;
    for (; i < arguments.length; i++) {
      var source = arguments[i];
      var prop;
      for (prop in source) {
        if (Object.prototype.hasOwnProperty.call(source, prop)) {
          target[prop] = source[prop];
        }
      }
    }
    return target;
  };
  var baseAssignValue = function() {
    /**
     * @param {!Function} d
     * @param {string} props
     * @return {undefined}
     */
    function t(d, props) {
      /** @type {number} */
      var i = 0;
      for (; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        /** @type {boolean} */
        descriptor.configurable = true;
        if ("value" in descriptor) {
          /** @type {boolean} */
          descriptor.writable = true;
        }
        Object.defineProperty(d, descriptor.key, descriptor);
      }
    }
    return function(p, n, a) {
      return n && t(p.prototype, n), a && t(p, a), p;
    };
  }();
  var _deepAssign2 = function(obj) {
    return obj && obj.__esModule ? obj : {
      default : obj
    };
  }(saveNotifs(0));
  var offsetFromCenter = function() {
    /**
     * @param {string} bShowType
     * @return {undefined}
     */
    function result(bShowType) {
      var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0;
      fn(this, result);
      /** @type {string} */
      this._hostname = bShowType;
      this._tabsQuery = {
        url : "*://" + bShowType + "/*"
      };
      this._debounceDelay = n;
      this._tabs = {};
      /** @type {boolean} */
      this._started = false;
      this._listeners = {
        onCreated : this._onTabCreated.bind(this),
        onUpdated : this._onTabUpdated.bind(this),
        onReplaced : this._onTabReplaced.bind(this),
        onRemoved : this._onTabRemoved.bind(this)
      };
    }
    return baseAssignValue(result, [{
      key : "start",
      value : function() {
        if (!this._started) {
          this._initTabs();
          var i;
          for (i in this._listeners) {
            chrome.tabs[i].addListener(this._listeners[i]);
          }
          /** @type {boolean} */
          this._started = true;
        }
      }
    }, {
      key : "stop",
      value : function() {
        if (this._started) {
          clearTimeout(this._debounceTimer);
          var i;
          for (i in this._listeners) {
            chrome.tabs[i].removeListener(this._listeners[i]);
          }
          /** @type {boolean} */
          this._started = false;
          if (_deepAssign2.default.loggingEnabled) {
            console.log("AudibleTabTitleTracker stopped");
          }
        }
      }
    }, {
      key : "_initTabs",
      value : function() {
        this._tabs = {};
        chrome.tabs.query(this._tabsQuery, this._initTabsFound.bind(this));
      }
    }, {
      key : "_initTabsFound",
      value : function(networks) {
        /** @type {boolean} */
        var _iteratorNormalCompletion3 = true;
        /** @type {boolean} */
        var n = false;
        var a = void 0;
        try {
          var info;
          var _iterator3 = networks[Symbol.iterator]();
          for (; !(_iteratorNormalCompletion3 = (info = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
            var u = info.value;
            this._setTrackedTab(u);
          }
        } catch (nativeObjectObject) {
          /** @type {boolean} */
          n = true;
          a = nativeObjectObject;
        } finally {
          try {
            if (!_iteratorNormalCompletion3 && _iterator3.return) {
              _iterator3.return();
            }
          } finally {
            if (n) {
              throw a;
            }
          }
        }
        this._checkForCurrentAudibleTabChange();
        if (_deepAssign2.default.loggingEnabled) {
          console.log("AudibleTabTitleTracker started:\n - host: " + this._hostname + "\n - debounce delay: " + this._debounceDelay + "\n - " + Object.keys(this._tabs).length + " tabs: " + JSON.stringify(this._tabs, null, 4));
        }
      }
    }, {
      key : "_hostnameMatches",
      value : function(url) {
        try {
          return (new URL(url)).hostname === this._hostname;
        } catch (e) {
          return false;
        }
      }
    }, {
      key : "_setTrackedTab",
      value : function(options) {
        this._tabs[options.id] = {
          id : options.id,
          title : options.title,
          audible : options.audible
        };
      }
    }, {
      key : "_onTabCreated",
      value : function(options) {
        if (this._hostnameMatches(options.url)) {
          this._setTrackedTab(options);
          if (options.audible) {
            this._checkForCurrentAudibleTabChange();
          }
        }
      }
    }, {
      key : "_onTabUpdated",
      value : function(id, options, n) {
        if (id in this._tabs) {
          /** @type {boolean} */
          var a = false;
          if ("url" in options && !this._hostnameMatches(n.url)) {
            delete this._tabs[id];
            /** @type {boolean} */
            a = true;
          } else {
            if ("title" in options) {
              this._tabs[id].title = options.title;
              /** @type {boolean} */
              a = true;
            }
            if ("audible" in options) {
              this._tabs[id].audible = options.audible;
              /** @type {boolean} */
              a = true;
            }
          }
          if (a) {
            this._checkForCurrentAudibleTabChange();
          }
        } else {
          this._onTabCreated(n);
        }
      }
    }, {
      key : "_onTabReplaced",
      value : function(id, index) {
        if (index in this._tabs) {
          this._tabs[id] = this._tabs[index];
          /** @type {string} */
          this._tabs[id].id = id;
          delete this._tabs[index];
          this._checkForCurrentAudibleTabChange();
        }
      }
    }, {
      key : "_onTabRemoved",
      value : function(id, att_id) {
        if (id in this._tabs) {
          delete this._tabs[id];
          this._checkForCurrentAudibleTabChange();
        }
      }
    }, {
      key : "_findCurrentAudibleTab",
      value : function() {
        return Object.values(this._tabs).find(function(options) {
          return options.audible;
        });
      }
    }, {
      key : "_checkForCurrentAudibleTabChange",
      value : function() {
        var e = this._findCurrentAudibleTab();
        if (e !== this._currentAudibleTab) {
          /** @type {(Object|undefined)} */
          this._currentAudibleTab = e ? $({}, e) : void 0;
          if (this._debounceDelay > 0) {
            clearTimeout(this._debounceTimer);
            /** @type {number} */
            this._debounceTimer = setTimeout(this._currentAudibleTabChanged.bind(this), this._debounceDelay);
          } else {
            this._currentAudibleTabChanged();
          }
        }
      }
    }, {
      key : "_currentAudibleTabChanged",
      value : function() {
        if ("function" == typeof this._onCurrentAudibleTabChange) {
          this._onCurrentAudibleTabChange(this._currentAudibleTab);
        }
      }
    }, {
      key : "isStarted",
      get : function() {
        return this._started;
      }
    }, {
      key : "currentAudibleTab",
      get : function() {
        return this._currentAudibleTab;
      }
    }, {
      key : "onCurrentAudibleTabChange",
      set : function(e) {
        this._onCurrentAudibleTabChange = e;
      }
    }]), result;
  }();
  t.default = offsetFromCenter;
}, function(canCreateDiscussions, t, saveNotifs) {
  /**
   * @param {!AudioNode} that
   * @param {!Function} size
   * @return {undefined}
   */
  function get(that, size) {
    if (!(that instanceof size)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  Object.defineProperty(t, "__esModule", {
    value : true
  });
  var installNativeEvent$2 = function() {
    /**
     * @param {!Function} d
     * @param {string} props
     * @return {undefined}
     */
    function t(d, props) {
      /** @type {number} */
      var i = 0;
      for (; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        /** @type {boolean} */
        descriptor.configurable = true;
        if ("value" in descriptor) {
          /** @type {boolean} */
          descriptor.writable = true;
        }
        Object.defineProperty(d, descriptor.key, descriptor);
      }
    }
    return function(p, n, a) {
      return n && t(p.prototype, n), a && t(p, a), p;
    };
  }();
  var o = function(obj) {
    return obj && obj.__esModule ? obj : {
      default : obj
    };
  }(saveNotifs(0));
  /** @type {string} */
  var platform = navigator.platform;
  var methodName = chrome.i18n.getMessage("appName");
  var offsetFromCenter = function() {
    /**
     * @param {string} val
     * @return {undefined}
     */
    function n(val) {
      var ms = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 500;
      var i = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 6E5;
      get(this, n);
      /** @type {string} */
      this._token = val;
      this._retryDelay = ms;
      this._closeDelay = i;
    }
    return installNativeEvent$2(n, [{
      key : "sendStatusUpdate",
      value : function(data) {
        var _this = this;
        clearTimeout(this._retryTimer);
        clearTimeout(this._closeTimer);
        if (this._ws && this._ws.readyState !== WebSocket.CLOSED) {
          if (this._ws.readyState === WebSocket.OPEN) {
            this._send(this._getOpStatusUpdatePayload(data));
          } else {
            if (!(this._ws.readyState !== WebSocket.CONNECTING && this._ws.readyState !== WebSocket.CLOSING)) {
              /** @type {number} */
              this._retryTimer = setTimeout(function() {
                return _this.sendStatusUpdate(data);
              }, this._retryDelay);
            }
          }
        } else {
          if (o.default.loggingEnabled) {
            console.log("Token " + this._token + ": initialize websocket...");
          }
          /** @type {!WebSocket} */
          this._ws = new WebSocket("wss://gateway.discord.gg/?v=6&encoding=json");
          /** @type {null} */
          this._lastSeq = null;
          /**
           * @param {?} s
           * @return {undefined}
           */
          this._ws.onopen = function(s) {
            _this._send(_this._getOpIdentifyPayload(data));
            _this._identifyStatus = data;
          };
          /**
           * @param {!Object} event
           * @return {?}
           */
          this._ws.onmessage = function(event) {
            return _this._messageHandler(JSON.parse(event.data));
          };
          /**
           * @param {?} event
           * @return {?}
           */
          this._ws.onerror = function(event) {
            return console.log("Token " + _this._token + ": connection error");
          };
          /**
           * @param {!Object} event
           * @return {undefined}
           */
          this._ws.onclose = function(event) {
            clearInterval(_this._heartbeatTimer);
            if (o.default.loggingEnabled) {
              console.log("Token " + _this._token + ": connection closed\n - code: " + event.code + "\n - reason: " + event.reason + "\n - wasClean: " + event.wasClean);
            }
          };
        }
        if (this._closeDelay > 0) {
          /** @type {number} */
          this._closeTimer = setTimeout(this.close, this._closeDelay);
        }
      }
    }, {
      key : "close",
      value : function() {
        clearTimeout(this._retryTimer);
        clearTimeout(this._closeTimer);
        if (!(!this._ws || this._ws.readyState !== WebSocket.OPEN && this._ws.readyState !== WebSocket.CONNECTING)) {
          this._ws.close();
        }
      }
    }, {
      key : "_send",
      value : function(data) {
        this._ws.send(JSON.stringify(data));
        if (o.default.loggingEnabled) {
          console.log("Token " + this._token + ": sent " + JSON.stringify(data, null, 4));
        }
      }
    }, {
      key : "_sendHeartbeat",
      value : function() {
        if (this._ws && this._ws.readyState === WebSocket.OPEN) {
          this._send(getOpHeartbeatPayload());
        }
      }
    }, {
      key : "_messageHandler",
      value : function(msg) {
        switch(this._lastSeq = msg.s, msg.op) {
          case 1:
            this._sendHeartbeat();
            if (o.default.loggingEnabled) {
              console.log("Token " + this._token + ": received heartbeat request message: " + JSON.stringify(msg, null, 4));
            }
            break;
          case 10:
            clearInterval(this._heartbeatTimer);
            /** @type {number} */
            this._heartbeatTimer = setInterval(this._sendHeartbeat, msg.d.heartbeat_interval);
            if (o.default.loggingEnabled) {
              console.log("Token " + this._token + ": received hello message: " + JSON.stringify(msg, null, 4));
            }
            if (!this._identifyStatus) {
              this._send(this._getOpStatusUpdatePayload(this._identifyStatus));
            }
        }
      }
    }, {
      key : "_getOpHeartbeatPayload",
      value : function() {
        return {
          op : 1,
          d : this._lastSeq
        };
      }
    }, {
      key : "_getStatusUpdatePayload",
      value : function(e) {
        return {
          game : null === e ? null : {
            name : e,
            type : 2
          },
          status : "online",
          since : null,
          afk : false
        };
      }
    }, {
      key : "_getOpIdentifyPayload",
      value : function(saveEvenIfSeemsUnchanged) {
        return {
          op : 2,
          d : {
            token : this._token,
            properties : {
              $os : platform,
              $browser : "Chrome",
              $device : methodName
            },
            compress : false,
            large_threshold : 50,
            presence : this._getStatusUpdatePayload(saveEvenIfSeemsUnchanged)
          }
        };
      }
    }, {
      key : "_getOpStatusUpdatePayload",
      value : function(data) {
        return {
          op : 3,
          d : this._getStatusUpdatePayload(data)
        };
      }
    }]), n;
  }();
  t.default = offsetFromCenter;
}, function(canCreateDiscussions, t, saveNotifs) {
  /**
   * @param {!AudioNode} element
   * @param {!Function} parent
   * @return {undefined}
   */
  function fn(element, parent) {
    if (!(element instanceof parent)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  Object.defineProperty(t, "__esModule", {
    value : true
  });
  var baseAssignValue = function() {
    /**
     * @param {!Function} d
     * @param {string} props
     * @return {undefined}
     */
    function t(d, props) {
      /** @type {number} */
      var i = 0;
      for (; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        /** @type {boolean} */
        descriptor.configurable = true;
        if ("value" in descriptor) {
          /** @type {boolean} */
          descriptor.writable = true;
        }
        Object.defineProperty(d, descriptor.key, descriptor);
      }
    }
    return function(p, n, a) {
      return n && t(p.prototype, n), a && t(p, a), p;
    };
  }();
  var findValidation = (function(exports) {
    if (exports) {
      exports.__esModule;
    }
  }(saveNotifs(0)), {
    url : "*://discord.com/*"
  });
  var offsetFromCenter = function() {
    /**
     * @return {undefined}
     */
    function result() {
      fn(this, result);
      /** @type {boolean} */
      this._started = false;
      this._listeners = {
        onCreated : this._onTabCreated.bind(this),
        onUpdated : this._onTabUpdated.bind(this)
      };
    }
    return baseAssignValue(result, [{
      key : "start",
      value : function() {
        if (!this._started) {
          this._findTabsTokens();
          var i;
          for (i in this._listeners) {
            chrome.tabs[i].addListener(this._listeners[i]);
          }
          /** @type {boolean} */
          this._started = true;
        }
      }
    }, {
      key : "stop",
      value : function() {
        if (this._started) {
          var i;
          for (i in this._listeners) {
            chrome.tabs[i].removeListener(this._listeners[i]);
          }
          /** @type {boolean} */
          this._started = false;
        }
      }
    }, {
      key : "_hostnameMatches",
      value : function(url) {
        try {
          return "discord.com" === (new URL(url)).hostname;
        } catch (e) {
          return false;
        }
      }
    }, {
      key : "_findTabsTokens",
      value : function() {
        var foreignControls = this;
        chrome.tabs.query(findValidation, function(wrappersTemplates) {
          return wrappersTemplates.forEach(function(reverseControl) {
            return foreignControls._checkTabForToken(reverseControl);
          });
        });
      }
    }, {
      key : "_checkTabForToken",
      value : function(data) {
        var touchSystem = this;
        chrome.tabs.executeScript(data.id, {
          code : "localStorage.getItem('token');"
        }, function(buildInTemplates) {
          return buildInTemplates.map(function(pStringValue) {
            return JSON.parse(pStringValue);
          }).filter(function(canCreateDiscussions) {
            return !!canCreateDiscussions;
          }).forEach(function(e) {
            return touchSystem._onFoundCallback(e);
          });
        });
      }
    }, {
      key : "_onTabCreated",
      value : function(e) {
        if (this._hostnameMatches(e.url)) {
          this._checkTabForToken(e);
        }
      }
    }, {
      key : "_onTabUpdated",
      value : function(name, commit, saveEvenIfSeemsUnchanged) {
        this._onTabCreated(saveEvenIfSeemsUnchanged);
      }
    }, {
      key : "isStarted",
      get : function() {
        return this._started;
      }
    }, {
      key : "onFound",
      set : function(e) {
        this._onFoundCallback = e;
      }
    }]), result;
  }();
  t.default = offsetFromCenter;
}]);
