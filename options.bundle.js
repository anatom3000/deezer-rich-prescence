/**
 * @license
 Apache-2.0
 @copyright 2015 Google, Inc.
 @link https://github.com/google/material-design-lite
*/
'use strict';
!function(m) {
  /**
   * @param {number} i
   * @return {?}
   */
  function t(i) {
    if (n[i]) {
      return n[i].exports;
    }
    var module = n[i] = {
      i : i,
      l : false,
      exports : {}
    };
    return m[i].call(module.exports, module, module.exports, t), module.l = true, module.exports;
  }
  var n = {};
  /** @type {!Array} */
  t.m = m;
  t.c = n;
  /**
   * @param {!Function} d
   * @param {string} name
   * @param {!Function} n
   * @return {undefined}
   */
  t.d = function(d, name, n) {
    if (!t.o(d, name)) {
      Object.defineProperty(d, name, {
        configurable : false,
        enumerable : true,
        get : n
      });
    }
  };
  /**
   * @param {!Object} module
   * @return {?}
   */
  t.n = function(module) {
    /** @type {function(): ?} */
    var n = module && module.__esModule ? function() {
      return module.default;
    } : function() {
      return module;
    };
    return t.d(n, "a", n), n;
  };
  /**
   * @param {!Function} t
   * @param {string} object
   * @return {?}
   */
  t.o = function(t, object) {
    return Object.prototype.hasOwnProperty.call(t, object);
  };
  /** @type {string} */
  t.p = "";
  t(t.s = 6);
}([function(canCreateDiscussions, option, s) {
  /**
   * @param {!AudioNode} a
   * @param {!Function} b
   * @return {undefined}
   */
  function emit(a, b) {
    if (!(a instanceof b)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  Object.defineProperty(option, "__esModule", {
    value : true
  });
  var _createClass = function() {
    /**
     * @param {!Function} d
     * @param {string} props
     * @return {undefined}
     */
    function t(d, props) {
      /** @type {number} */
      var i = 0;
      for (; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        /** @type {boolean} */
        descriptor.configurable = true;
        if ("value" in descriptor) {
          /** @type {boolean} */
          descriptor.writable = true;
        }
        Object.defineProperty(d, descriptor.key, descriptor);
      }
    }
    return function(p, n, a) {
      return n && t(p.prototype, n), a && t(p, a), p;
    };
  }();
  var Buffer = function() {
    /**
     * @return {undefined}
     */
    function e() {
      emit(this, e);
      /** @type {string} */
      this._tokensPolicy = "last";
      /** @type {null} */
      this._lastTokenFound = null;
      /** @type {!Array} */
      this._tokensFound = [];
      /** @type {!Array} */
      this._tokensSelection = [];
      /** @type {boolean} */
      this._notificationsEnabled = true;
      /** @type {boolean} */
      this._loggingEnabled = true;
    }
    return _createClass(e, [{
      key : "load",
      value : function() {
        var obj = this;
        return new Promise(function(saveNotifs) {
          /** @type {!Array<?>} */
          var nodeIds = Object.keys(obj).map(function(OldString) {
            return OldString.substring(1);
          });
          chrome.storage.sync.get(nodeIds, function(options) {
            Object.assign(obj, options);
            var changed = {};
            /** @type {boolean} */
            var _iteratorNormalCompletion3 = true;
            /** @type {boolean} */
            var l = false;
            var o = void 0;
            try {
              var _step3;
              var _iterator3 = nodeIds[Symbol.iterator]();
              for (; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                var name = _step3.value;
                changed[name] = {
                  oldValue : obj["_" + name],
                  newValue : options[name] ? options[name] : obj["_" + name]
                };
              }
            } catch (tObj) {
              /** @type {boolean} */
              l = true;
              o = tObj;
            } finally {
              try {
                if (!_iteratorNormalCompletion3 && _iterator3.return) {
                  _iterator3.return();
                }
              } finally {
                if (l) {
                  throw o;
                }
              }
            }
            if (obj._onChangedCallback) {
              obj._onChangedCallback(changed);
            }
            if (obj.loggingEnabled) {
              console.log("Config set to: " + JSON.stringify(obj, null, 4));
            }
            chrome.storage.onChanged.addListener(function(params, s) {
              /** @type {boolean} */
              var isProperty = false;
              var i;
              for (i in params) {
                if (obj["_" + i] !== params[i].newValue) {
                  obj["_" + i] = params[i].newValue;
                  /** @type {boolean} */
                  isProperty = true;
                  if (obj._loggingEnabled || "loggingEnabled" === i) {
                    console.log("Config '" + i + "' changed to " + params[i].newValue + ".");
                  }
                }
              }
              if (isProperty && obj._onChangedCallback) {
                obj._onChangedCallback(params);
              }
            });
            saveNotifs();
          });
        });
      }
    }, {
      key : "notificationsEnabled",
      get : function() {
        return this._notificationsEnabled;
      },
      set : function(data) {
        if (this._notificationsEnabled !== data) {
          var artistTrack = {
            notificationsEnabled : {
              oldValue : this._notificationsEnabled,
              newValue : data
            }
          };
          /** @type {string} */
          this._notificationsEnabled = data;
          chrome.storage.sync.set({
            notificationsEnabled : data
          });
          if (this._onChangedCallback) {
            this._onChangedCallback(artistTrack);
          }
        }
      }
    }, {
      key : "lastTokenFound",
      get : function() {
        return this._lastTokenFound;
      },
      set : function(data) {
        if (this._lastTokenFound !== data) {
          var artistTrack = {
            lastTokenFound : {
              oldValue : this._lastTokenFound,
              newValue : data
            }
          };
          /** @type {!Object} */
          this._lastTokenFound = data;
          chrome.storage.sync.set({
            lastTokenFound : data
          });
          if (this._onChangedCallback) {
            this._onChangedCallback(artistTrack);
          }
        }
      }
    }, {
      key : "tokensPolicy",
      get : function() {
        return this._tokensPolicy;
      },
      set : function(data) {
        if (this._tokensPolicy !== data) {
          var artistTrack = {
            tokensPolicy : {
              oldValue : this._tokensPolicy,
              newValue : data
            }
          };
          /** @type {string} */
          this._tokensPolicy = data;
          chrome.storage.sync.set({
            tokensPolicy : data
          });
          if (this._onChangedCallback) {
            this._onChangedCallback(artistTrack);
          }
        }
      }
    }, {
      key : "tokensFound",
      get : function() {
        return this._tokensFound;
      },
      set : function(data) {
        if (this._tokensFound !== data) {
          var artistTrack = {
            tokensFound : {
              oldValue : this._tokensFound,
              newValue : data
            }
          };
          /** @type {string} */
          this._tokensFound = data;
          chrome.storage.sync.set({
            tokensFound : data
          });
          if (this._onChangedCallback) {
            this._onChangedCallback(artistTrack);
          }
        }
      }
    }, {
      key : "tokensSelection",
      get : function() {
        return this._tokensSelection;
      },
      set : function(data) {
        if (this._tokensSelection !== data) {
          var artistTrack = {
            tokensSelection : {
              oldValue : this._tokensSelection,
              newValue : data
            }
          };
          /** @type {!Object} */
          this._tokensSelection = data;
          chrome.storage.sync.set({
            tokensSelection : data
          });
          if (this._onChangedCallback) {
            this._onChangedCallback(artistTrack);
          }
        }
      }
    }, {
      key : "loggingEnabled",
      get : function() {
        return this._loggingEnabled;
      },
      set : function(data) {
        if (this._loggingEnabled !== data) {
          var actual = {
            loggingEnabled : {
              oldValue : this._loggingEnabled,
              newValue : data
            }
          };
          /** @type {!Object} */
          this._loggingEnabled = data;
          chrome.storage.sync.set({
            loggingEnabled : data
          });
          if (this._onChangedCallback) {
            this._onChangedCallback(actual);
          }
        }
      }
    }, {
      key : "onChanged",
      set : function(e) {
        /** @type {boolean} */
        this._onChangedCallback = e;
      }
    }]), e;
  }();
  option.default = new Buffer;
}, , , , , , function(canCreateDiscussions, isSlidingUp, __webpack_require__) {
  /**
   * @param {!Object} obj
   * @return {?}
   */
  function _interopRequire(obj) {
    return obj && obj.__esModule ? obj : {
      default : obj
    };
  }
  /**
   * @param {string} method
   * @return {undefined}
   */
  function __request(method) {
    /** @type {!XMLHttpRequest} */
    var xhr = new XMLHttpRequest;
    xhr.open("GET", GROUPS_END_POINT, true);
    xhr.setRequestHeader("Authorization", method);
    xhr.setRequestHeader("Content-Type", "application/json");
    /**
     * @return {undefined}
     */
    xhr.onload = function() {
      if (200 === xhr.status) {
        /** @type {*} */
        var user = JSON.parse(xhr.responseText);
        /** @type {(Element|null)} */
        var lnkDiv = document.querySelector('.mdl-checkbox input[value="' + method + '"] + .mdl-checkbox__label');
        if (lnkDiv) {
          /** @type {string} */
          lnkDiv.innerHTML = user.username + "#" + user.discriminator;
        }
        if (o.innerHTML === "(" + method + ")") {
          /** @type {string} */
          o.innerHTML = "(" + user.username + "#" + user.discriminator + ")";
        }
      }
    };
    /** @type {string} */
    var loginMsg = JSON.stringify({
      content : "get infos"
    });
    xhr.send(loginMsg);
  }
  var config = _interopRequire(__webpack_require__(0));
  _interopRequire(__webpack_require__(7));
  !function() {
    /** @type {!NodeList<Element>} */
    var vmArgSetters = document.getElementsByTagName("html");
    /** @type {boolean} */
    var _iteratorNormalCompletion3 = true;
    /** @type {boolean} */
    var s = false;
    var i = void 0;
    try {
      var $__6;
      var _iterator3 = vmArgSetters[Symbol.iterator]();
      for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
        var item = $__6.value;
        var currentID = item.innerHTML.toString();
        var i = currentID.replace(/__MSG_(\w+)__/g, function(canCreateDiscussions, val) {
          return val ? chrome.i18n.getMessage(val) : "";
        });
        if (i != currentID) {
          item.innerHTML = i;
        }
      }
    } catch (contactCapacity) {
      /** @type {boolean} */
      s = true;
      i = contactCapacity;
    } finally {
      try {
        if (!_iteratorNormalCompletion3 && _iterator3.return) {
          _iterator3.return();
        }
      } finally {
        if (s) {
          throw i;
        }
      }
    }
  }();
  /** @type {string} */
  var id = 'input[type="radio"][name="tokens_policy"]';
  /** @type {(Element|null)} */
  var o = document.getElementById("last_token_found");
  /** @type {string} */
  var GROUPS_END_POINT = "https://discord.com/api/users/@me";
  document.addEventListener("DOMContentLoaded", function() {
    /** @type {(Element|null)} */
    var e = document.getElementById("notifications_enabled");
    /** @type {(Element|null)} */
    var t = document.getElementById("logging_enabled");
    /** @type {!NodeList<Element>} */
    var elts = document.querySelectorAll(id);
    /** @type {(Element|null)} */
    var self = document.getElementById("tokens_selection");
    /**
     * @param {!Object} prefs
     * @return {undefined}
     */
    config.default.onChanged = function(prefs) {
      console.log("changeInfo: " + JSON.stringify(prefs, null, 4));
      var key;
      for (key in prefs) {
        !function(name) {
          var v = prefs[name].newValue;
          switch(name) {
            case "notificationsEnabled":
              e.checked = v;
              if (e.parentElement.MaterialCheckbox) {
                if (v) {
                  e.parentElement.MaterialCheckbox.check();
                } else {
                  e.parentElement.MaterialCheckbox.uncheck();
                }
              }
              break;
            case "loggingEnabled":
              t.checked = v;
              if (t.parentElement.MaterialCheckbox) {
                if (v) {
                  t.parentElement.MaterialCheckbox.check();
                } else {
                  t.parentElement.MaterialCheckbox.uncheck();
                }
              }
              break;
            case "lastTokenFound":
              /** @type {string} */
              o.innerHTML = v ? "(" + v + ")" : "";
              __request(v);
              break;
            case "tokensPolicy":
              console.log(id + '[value="' + v + '"] checked -> true');
              /** @type {(Element|null)} */
              var guiWidget = document.querySelector(".mdl-radio > " + id + '[value="' + v + '"]');
              if (guiWidget) {
                /** @type {boolean} */
                guiWidget.checked = true;
                if (guiWidget.parentElement.MaterialRadio) {
                  guiWidget.parentElement.MaterialRadio.check();
                }
              }
              break;
            case "tokensFound":
              if (0 === v.length) {
                /** @type {string} */
                self.innerHTML = '\n            <li class="mdl-list__item">\n              <span class="mdl-list__item-primary-content">\n                ' + chrome.i18n.getMessage("tokensNoneDetected") + ',&nbsp;<a href="https://discord.com/channels/@me" target="_blank">' + chrome.i18n.getMessage("tokensConnectToDiscord") + "</a>\n              </span>\n            </li>\n          ";
              } else {
                self.innerHTML = v.map(function(canCreateDiscussions) {
                  return '\n            <li class="mdl-list__item">\n              <label class="mdl-list__item-primary-content mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect">\n                <input type="checkbox" class="mdl-checkbox__input" value="' + canCreateDiscussions + '">\n                <span class="mdl-checkbox__label">' + canCreateDiscussions + '</span>\n                <button class="mdl-button mdl-js-button mdl-button--icon">\n                  <i class="material-icons">delete</i>\n                </button>\n              </label>\n            </li>\n          ';
                }).join("");
                if (componentHandler) {
                  componentHandler.upgradeDom();
                }
                v.forEach(function(method) {
                  __request(method);
                  /** @type {(Element|null)} */
                  var h = document.querySelector('.mdl-checkbox input[value="' + method + '"]');
                  h.checked = config.default.tokensSelection.includes(h.value);
                  if (h.parentElement.MaterialCheckbox) {
                    if (h.checked) {
                      h.parentElement.MaterialCheckbox.check();
                    } else {
                      h.parentElement.MaterialCheckbox.uncheck();
                    }
                  }
                  /**
                   * @param {?} inDisplayValue
                   * @return {undefined}
                   */
                  h.onchange = function(inDisplayValue) {
                    if (h.checked) {
                      if (!config.default.tokensSelection.includes(method)) {
                        var calledMethods = config.default.tokensSelection.slice();
                        calledMethods.push(method);
                        config.default.tokensSelection = calledMethods;
                      }
                    } else {
                      if (config.default.tokensSelection.includes(method)) {
                        config.default.tokensSelection = config.default.tokensSelection.filter(function(action) {
                          return action !== method;
                        });
                      }
                    }
                  };
                  /**
                   * @param {?} branch
                   * @return {undefined}
                   */
                  document.querySelector('.mdl-checkbox input[value="' + method + '"] ~ button').onclick = function(branch) {
                    if (config.default.lastTokenFound === method) {
                      /** @type {null} */
                      config.default.lastTokenFound = null;
                    }
                    config.default.tokensFound = config.default.tokensFound.filter(function(action) {
                      return action !== method;
                    });
                    config.default.tokensSelection = config.default.tokensSelection.filter(function(action) {
                      return action !== method;
                    });
                  };
                });
              }
              break;
            case "tokensSelection":
              document.querySelectorAll('#tokens_selection .mdl-checkbox input[type="checkbox"]').forEach(function(r) {
                r.checked = v.includes(r.value);
                if (r.parentElement.MaterialCheckbox) {
                  if (r.checked) {
                    r.parentElement.MaterialCheckbox.check();
                  } else {
                    r.parentElement.MaterialCheckbox.uncheck();
                  }
                }
              });
          }
        }(key);
      }
    };
    config.default.load().then(function() {
      console.log("config loaded");
      /**
       * @param {?} inDisplayValue
       * @return {undefined}
       */
      e.onchange = function(inDisplayValue) {
        console.log("notificationsEnabledCB.onchange");
        config.default.notificationsEnabled = e.checked;
      };
      /**
       * @param {?} inDisplayValue
       * @return {undefined}
       */
      t.onchange = function(inDisplayValue) {
        console.log("loggingEnabledCB.onchange");
        config.default.loggingEnabled = t.checked;
      };
      /** @type {boolean} */
      var _iteratorNormalCompletion3 = true;
      /** @type {boolean} */
      var o = false;
      var r = void 0;
      try {
        var t;
        var _iterator3 = elts[Symbol.iterator]();
        for (; !(_iteratorNormalCompletion3 = (t = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
          /**
           * @param {?} inDisplayValue
           * @return {undefined}
           */
          t.value.onchange = function(inDisplayValue) {
            var cwd = document.querySelector(id + ":checked").value;
            console.log("tokens policy set to " + cwd);
            config.default.tokensPolicy = cwd;
            /** @type {boolean} */
            self.disabled = "selection" !== cwd;
          };
        }
      } catch (G__20648) {
        /** @type {boolean} */
        o = true;
        r = G__20648;
      } finally {
        try {
          if (!_iteratorNormalCompletion3 && _iterator3.return) {
            _iterator3.return();
          }
        } finally {
          if (o) {
            throw r;
          }
        }
      }
    });
  });
}, function(canCreateDiscussions, isSlidingUp) {
  !function() {
    /**
     * @param {!Element} e
     * @param {!Object} ctx
     * @return {undefined}
     */
    function attachClickEvents(e, ctx) {
      if (e) {
        if (ctx.element_.classList.contains(ctx.CssClasses_.MDL_JS_RIPPLE_EFFECT)) {
          /** @type {!Element} */
          var s = document.createElement("span");
          s.classList.add(ctx.CssClasses_.MDL_RIPPLE_CONTAINER);
          s.classList.add(ctx.CssClasses_.MDL_JS_RIPPLE_EFFECT);
          /** @type {!Element} */
          var i = document.createElement("span");
          i.classList.add(ctx.CssClasses_.MDL_RIPPLE);
          s.appendChild(i);
          e.appendChild(s);
        }
        e.addEventListener("click", function(event) {
          if ("#" === e.getAttribute("href").charAt(0)) {
            event.preventDefault();
            var hostname_id = e.href.split("#")[1];
            var dayEle = ctx.element_.querySelector("#" + hostname_id);
            ctx.resetTabState_();
            ctx.resetPanelState_();
            e.classList.add(ctx.CssClasses_.ACTIVE_CLASS);
            dayEle.classList.add(ctx.CssClasses_.ACTIVE_CLASS);
          }
        });
      }
    }
    /**
     * @param {!Object} target
     * @param {(Node|NodeList|string)} a
     * @param {(Node|NodeList|string)} options
     * @param {?} layout
     * @return {undefined}
     */
    function t(target, a, options, layout) {
      /**
       * @return {undefined}
       */
      function done() {
        var hostname_id = target.href.split("#")[1];
        var dayEle = layout.content_.querySelector("#" + hostname_id);
        layout.resetTabState_(a);
        layout.resetPanelState_(options);
        target.classList.add(layout.CssClasses_.IS_ACTIVE);
        dayEle.classList.add(layout.CssClasses_.IS_ACTIVE);
      }
      if (layout.tabBar_.classList.contains(layout.CssClasses_.JS_RIPPLE_EFFECT)) {
        /** @type {!Element} */
        var a = document.createElement("span");
        a.classList.add(layout.CssClasses_.RIPPLE_CONTAINER);
        a.classList.add(layout.CssClasses_.JS_RIPPLE_EFFECT);
        /** @type {!Element} */
        var l = document.createElement("span");
        l.classList.add(layout.CssClasses_.RIPPLE);
        a.appendChild(l);
        target.appendChild(a);
      }
      if (!layout.tabBar_.classList.contains(layout.CssClasses_.TAB_MANUAL_SWITCH)) {
        target.addEventListener("click", function(event) {
          if ("#" === target.getAttribute("href").charAt(0)) {
            event.preventDefault();
            done();
          }
        });
      }
      /** @type {function(): undefined} */
      target.show = done;
    }
    var self = {
      upgradeDom : function(optJsClass, optCssClass) {
      },
      upgradeElement : function(element, context) {
      },
      upgradeElements : function(elements) {
      },
      upgradeAllRegistered : function() {
      },
      registerUpgradedCallback : function(jsClass, callback) {
      },
      register : function(inBases) {
      },
      downgradeElements : function(nodes) {
      }
    };
    (self = function() {
      /**
       * @param {string} name
       * @param {number} opt_replace
       * @return {?}
       */
      function findRegisteredClass_(name, opt_replace) {
        /** @type {number} */
        var i = 0;
        for (; i < registeredComponents_.length; i++) {
          if (registeredComponents_[i].className === name) {
            return void 0 !== opt_replace && (registeredComponents_[i] = opt_replace), registeredComponents_[i];
          }
        }
        return false;
      }
      /**
       * @param {!Element} element
       * @return {?}
       */
      function flatten(element) {
        var dataUpgraded = element.getAttribute("data-upgraded");
        return null === dataUpgraded ? [""] : dataUpgraded.split(",");
      }
      /**
       * @param {!Element} element
       * @param {string} text
       * @return {?}
       */
      function contains(element, text) {
        return -1 !== flatten(element).indexOf(text);
      }
      /**
       * @param {string} event
       * @param {boolean} bubbles
       * @param {boolean} cancelable
       * @return {?}
       */
      function createEvent(event, bubbles, cancelable) {
        if ("CustomEvent" in window && "function" == typeof window.CustomEvent) {
          return new CustomEvent(event, {
            bubbles : bubbles,
            cancelable : cancelable
          });
        }
        /** @type {(Event|null)} */
        var evt = document.createEvent("Events");
        return evt.initEvent(event, bubbles, cancelable), evt;
      }
      /**
       * @param {!Array} selector
       * @param {string} cssClass
       * @return {undefined}
       */
      function upgradeDomInternal(selector, cssClass) {
        if (void 0 === selector && void 0 === cssClass) {
          /** @type {number} */
          var i = 0;
          for (; i < registeredComponents_.length; i++) {
            upgradeDomInternal(registeredComponents_[i].className, registeredComponents_[i].cssClass);
          }
        } else {
          /** @type {!Array} */
          var callback = selector;
          if (void 0 === cssClass) {
            var registeredClass = findRegisteredClass_(callback);
            if (registeredClass) {
              cssClass = registeredClass.cssClass;
            }
          }
          /** @type {!NodeList<Element>} */
          var childNodes = document.querySelectorAll("." + cssClass);
          /** @type {number} */
          var i = 0;
          for (; i < childNodes.length; i++) {
            update(childNodes[i], callback);
          }
        }
      }
      /**
       * @param {!Element} element
       * @param {string} name
       * @return {undefined}
       */
      function update(element, name) {
        if (!("object" == typeof element && element instanceof Element)) {
          throw new Error("Invalid argument provided to upgrade MDL element.");
        }
        var evt = createEvent("mdl-componentupgrading", true, true);
        if (element.dispatchEvent(evt), !evt.defaultPrevented) {
          var r = flatten(element);
          /** @type {!Array} */
          var self = [];
          if (name) {
            if (!contains(element, name)) {
              self.push(findRegisteredClass_(name));
            }
          } else {
            /** @type {!DOMTokenList} */
            var c = element.classList;
            registeredComponents_.forEach(function(item) {
              if (c.contains(item.cssClass) && -1 === self.indexOf(item) && !contains(element, item.className)) {
                self.push(item);
              }
            });
          }
          var item;
          /** @type {number} */
          var i = 0;
          /** @type {number} */
          var nbConnections = self.length;
          for (; i < nbConnections; i++) {
            if (!(item = self[i])) {
              throw new Error("Unable to find a registered component for the given class.");
            }
            r.push(item.className);
            element.setAttribute("data-upgraded", r.join(","));
            var instance = new item.classConstructor(element);
            instance[componentConfigProperty_] = item;
            d.push(instance);
            /** @type {number} */
            var k = 0;
            var tileSetCount = item.callbacks.length;
            for (; k < tileSetCount; k++) {
              item.callbacks[k](element);
            }
            if (item.widget) {
              element[item.className] = instance;
            }
            var evt = createEvent("mdl-componentupgraded", true, false);
            element.dispatchEvent(evt);
          }
        }
      }
      /**
       * @param {string} elements
       * @return {undefined}
       */
      function push(elements) {
        if (!Array.isArray(elements)) {
          /** @type {!Array} */
          elements = elements instanceof Element ? [elements] : Array.prototype.slice.call(elements);
        }
        var element;
        /** @type {number} */
        var i = 0;
        var eL = elements.length;
        for (; i < eL; i++) {
          if ((element = elements[i]) instanceof HTMLElement) {
            update(element);
            if (element.children.length > 0) {
              push(element.children);
            }
          }
        }
      }
      /**
       * @param {!Object} component
       * @return {undefined}
       */
      function deconstructComponentInternal(component) {
        if (component) {
          /** @type {number} */
          var t = d.indexOf(component);
          d.splice(t, 1);
          var upgrades = component.element_.getAttribute("data-upgraded").split(",");
          var componentPlace = upgrades.indexOf(component[componentConfigProperty_].classAsString);
          upgrades.splice(componentPlace, 1);
          component.element_.setAttribute("data-upgraded", upgrades.join(","));
          var unprefixedEvent = createEvent("mdl-componentdowngraded", true, false);
          component.element_.dispatchEvent(unprefixedEvent);
        }
      }
      /** @type {!Array} */
      var registeredComponents_ = [];
      /** @type {!Array} */
      var d = [];
      /** @type {string} */
      var componentConfigProperty_ = "mdlComponentConfigInternal_";
      return {
        upgradeDom : upgradeDomInternal,
        upgradeElement : update,
        upgradeElements : push,
        upgradeAllRegistered : function() {
          /** @type {number} */
          var i = 0;
          for (; i < registeredComponents_.length; i++) {
            upgradeDomInternal(registeredComponents_[i].className);
          }
        },
        registerUpgradedCallback : function(jsClass, callback) {
          var regClass = findRegisteredClass_(jsClass);
          if (regClass) {
            regClass.callbacks.push(callback);
          }
        },
        register : function(config) {
          /** @type {boolean} */
          var rasterFunctionInstance = true;
          if (!(void 0 === config.widget && void 0 === config.widget)) {
            rasterFunctionInstance = config.widget || config.widget;
          }
          var newConfig = {
            classConstructor : config.constructor || config.constructor,
            className : config.classAsString || config.classAsString,
            cssClass : config.cssClass || config.cssClass,
            widget : rasterFunctionInstance,
            callbacks : []
          };
          if (registeredComponents_.forEach(function(item) {
            if (item.cssClass === newConfig.cssClass) {
              throw new Error("The provided cssClass has already been registered: " + item.cssClass);
            }
            if (item.className === newConfig.className) {
              throw new Error("The provided className has already been registered");
            }
          }), config.constructor.prototype.hasOwnProperty(componentConfigProperty_)) {
            throw new Error("MDL component classes must not have " + componentConfigProperty_ + " defined as a property.");
          }
          if (!findRegisteredClass_(config.classAsString, newConfig)) {
            registeredComponents_.push(newConfig);
          }
        },
        downgradeElements : function(obj) {
          /**
           * @param {!Object} instance
           * @return {undefined}
           */
          var t = function(instance) {
            d.filter(function(child) {
              return child.element_ === instance;
            }).forEach(deconstructComponentInternal);
          };
          if (obj instanceof Array || obj instanceof NodeList) {
            /** @type {number} */
            var i = 0;
            for (; i < obj.length; i++) {
              t(obj[i]);
            }
          } else {
            if (!(obj instanceof Node)) {
              throw new Error("Invalid argument provided to downgrade MDL nodes.");
            }
            t(obj);
          }
        }
      };
    }()).ComponentConfigPublic;
    self.ComponentConfig;
    self.Component;
    /** @type {function(?, ?): undefined} */
    self.upgradeDom = self.upgradeDom;
    /** @type {function(!Node, string): undefined} */
    self.upgradeElement = self.upgradeElement;
    /** @type {function(?): undefined} */
    self.upgradeElements = self.upgradeElements;
    /** @type {function(): undefined} */
    self.upgradeAllRegistered = self.upgradeAllRegistered;
    /** @type {function(?, ?): undefined} */
    self.registerUpgradedCallback = self.registerUpgradedCallback;
    /** @type {function(?): undefined} */
    self.register = self.register;
    /** @type {function(?): undefined} */
    self.downgradeElements = self.downgradeElements;
    window.componentHandler = self;
    window.componentHandler = self;
    window.addEventListener("load", function() {
      if ("classList" in document.createElement("div") && "querySelector" in document && "addEventListener" in window && Array.prototype.forEach) {
        document.documentElement.classList.add("mdl-js");
        self.upgradeAllRegistered();
      } else {
        /**
         * @return {undefined}
         */
        self.upgradeElement = function() {
        };
        /**
         * @return {undefined}
         */
        self.register = function() {
        };
      }
    });
    if (!Date.now) {
      /**
       * @return {number}
       */
      Date.now = function() {
        return (new Date).getTime();
      };
      Date.now = Date.now;
    }
    /** @type {!Array} */
    var vendors = ["webkit", "moz"];
    /** @type {number} */
    var i = 0;
    for (; i < vendors.length && !window.requestAnimationFrame; ++i) {
      var vp = vendors[i];
      window.requestAnimationFrame = window[vp + "RequestAnimationFrame"];
      window.cancelAnimationFrame = window[vp + "CancelAnimationFrame"] || window[vp + "CancelRequestAnimationFrame"];
      window.requestAnimationFrame = window.requestAnimationFrame;
      window.cancelAnimationFrame = window.cancelAnimationFrame;
    }
    if (/iP(ad|hone|od).*OS 6/.test(window.navigator.userAgent) || !window.requestAnimationFrame || !window.cancelAnimationFrame) {
      /** @type {number} */
      var previousTime = 0;
      /**
       * @param {!Function} callback
       * @return {?}
       */
      window.requestAnimationFrame = function(callback) {
        /** @type {number} */
        var immediateTime = Date.now();
        /** @type {number} */
        var lapsedTime = Math.max(previousTime + 16, immediateTime);
        return setTimeout(function() {
          callback(previousTime = lapsedTime);
        }, lapsedTime - immediateTime);
      };
      /** @type {function((null|number|undefined)): undefined} */
      window.cancelAnimationFrame = clearTimeout;
      /** @type {function(!Function): ?} */
      window.requestAnimationFrame = window.requestAnimationFrame;
      /** @type {function((null|number|undefined)): undefined} */
      window.cancelAnimationFrame = window.cancelAnimationFrame;
    }
    /**
     * @param {!Element} element
     * @return {undefined}
     */
    var Dropzone = function(element) {
      /** @type {!Element} */
      this.element_ = element;
      this.init();
    };
    /** @type {function(!Element): undefined} */
    window.MaterialButton = Dropzone;
    Dropzone.prototype.Constant_ = {};
    Dropzone.prototype.CssClasses_ = {
      RIPPLE_EFFECT : "mdl-js-ripple-effect",
      RIPPLE_CONTAINER : "mdl-button__ripple-container",
      RIPPLE : "mdl-ripple"
    };
    /**
     * @param {?} canCreateDiscussions
     * @return {undefined}
     */
    Dropzone.prototype.blurHandler_ = function(canCreateDiscussions) {
      if (canCreateDiscussions) {
        this.element_.blur();
      }
    };
    /**
     * @return {undefined}
     */
    Dropzone.prototype.disable = function() {
      /** @type {boolean} */
      this.element_.disabled = true;
    };
    /** @type {function(): undefined} */
    Dropzone.prototype.disable = Dropzone.prototype.disable;
    /**
     * @return {undefined}
     */
    Dropzone.prototype.enable = function() {
      /** @type {boolean} */
      this.element_.disabled = false;
    };
    /** @type {function(): undefined} */
    Dropzone.prototype.enable = Dropzone.prototype.enable;
    /**
     * @return {undefined}
     */
    Dropzone.prototype.init = function() {
      if (this.element_) {
        if (this.element_.classList.contains(this.CssClasses_.RIPPLE_EFFECT)) {
          /** @type {!Element} */
          var rippleContainer = document.createElement("span");
          rippleContainer.classList.add(this.CssClasses_.RIPPLE_CONTAINER);
          /** @type {!Element} */
          this.rippleElement_ = document.createElement("span");
          this.rippleElement_.classList.add(this.CssClasses_.RIPPLE);
          rippleContainer.appendChild(this.rippleElement_);
          this.boundRippleBlurHandler = this.blurHandler_.bind(this);
          this.rippleElement_.addEventListener("mouseup", this.boundRippleBlurHandler);
          this.element_.appendChild(rippleContainer);
        }
        this.boundButtonBlurHandler = this.blurHandler_.bind(this);
        this.element_.addEventListener("mouseup", this.boundButtonBlurHandler);
        this.element_.addEventListener("mouseleave", this.boundButtonBlurHandler);
      }
    };
    self.register({
      constructor : Dropzone,
      classAsString : "MaterialButton",
      cssClass : "mdl-js-button",
      widget : true
    });
    /**
     * @param {!Element} element
     * @return {undefined}
     */
    var MaterialSelectfield = function(element) {
      /** @type {!Element} */
      this.element_ = element;
      this.init();
    };
    /** @type {function(!Element): undefined} */
    window.MaterialCheckbox = MaterialSelectfield;
    MaterialSelectfield.prototype.Constant_ = {
      TINY_TIMEOUT : .001
    };
    MaterialSelectfield.prototype.CssClasses_ = {
      INPUT : "mdl-checkbox__input",
      BOX_OUTLINE : "mdl-checkbox__box-outline",
      FOCUS_HELPER : "mdl-checkbox__focus-helper",
      TICK_OUTLINE : "mdl-checkbox__tick-outline",
      RIPPLE_EFFECT : "mdl-js-ripple-effect",
      RIPPLE_IGNORE_EVENTS : "mdl-js-ripple-effect--ignore-events",
      RIPPLE_CONTAINER : "mdl-checkbox__ripple-container",
      RIPPLE_CENTER : "mdl-ripple--center",
      RIPPLE : "mdl-ripple",
      IS_FOCUSED : "is-focused",
      IS_DISABLED : "is-disabled",
      IS_CHECKED : "is-checked",
      IS_UPGRADED : "is-upgraded"
    };
    /**
     * @param {!Object} delta
     * @return {undefined}
     */
    MaterialSelectfield.prototype.onChange_ = function(delta) {
      this.updateClasses_();
    };
    /**
     * @param {?} event
     * @return {undefined}
     */
    MaterialSelectfield.prototype.onFocus_ = function(event) {
      this.element_.classList.add(this.CssClasses_.IS_FOCUSED);
    };
    /**
     * @param {?} opt_e
     * @return {undefined}
     */
    MaterialSelectfield.prototype.onBlur_ = function(opt_e) {
      this.element_.classList.remove(this.CssClasses_.IS_FOCUSED);
    };
    /**
     * @param {?} event
     * @return {undefined}
     */
    MaterialSelectfield.prototype.onMouseUp_ = function(event) {
      this.blur_();
    };
    /**
     * @return {undefined}
     */
    MaterialSelectfield.prototype.updateClasses_ = function() {
      this.checkDisabled();
      this.checkToggleState();
    };
    /**
     * @return {undefined}
     */
    MaterialSelectfield.prototype.blur_ = function() {
      window.setTimeout(function() {
        this.inputElement_.blur();
      }.bind(this), this.Constant_.TINY_TIMEOUT);
    };
    /**
     * @return {undefined}
     */
    MaterialSelectfield.prototype.checkToggleState = function() {
      if (this.inputElement_.checked) {
        this.element_.classList.add(this.CssClasses_.IS_CHECKED);
      } else {
        this.element_.classList.remove(this.CssClasses_.IS_CHECKED);
      }
    };
    /** @type {function(): undefined} */
    MaterialSelectfield.prototype.checkToggleState = MaterialSelectfield.prototype.checkToggleState;
    /**
     * @return {undefined}
     */
    MaterialSelectfield.prototype.checkDisabled = function() {
      if (this.inputElement_.disabled) {
        this.element_.classList.add(this.CssClasses_.IS_DISABLED);
      } else {
        this.element_.classList.remove(this.CssClasses_.IS_DISABLED);
      }
    };
    /** @type {function(): undefined} */
    MaterialSelectfield.prototype.checkDisabled = MaterialSelectfield.prototype.checkDisabled;
    /**
     * @return {undefined}
     */
    MaterialSelectfield.prototype.disable = function() {
      /** @type {boolean} */
      this.inputElement_.disabled = true;
      this.updateClasses_();
    };
    /** @type {function(): undefined} */
    MaterialSelectfield.prototype.disable = MaterialSelectfield.prototype.disable;
    /**
     * @return {undefined}
     */
    MaterialSelectfield.prototype.enable = function() {
      /** @type {boolean} */
      this.inputElement_.disabled = false;
      this.updateClasses_();
    };
    /** @type {function(): undefined} */
    MaterialSelectfield.prototype.enable = MaterialSelectfield.prototype.enable;
    /**
     * @return {undefined}
     */
    MaterialSelectfield.prototype.check = function() {
      /** @type {boolean} */
      this.inputElement_.checked = true;
      this.updateClasses_();
    };
    /** @type {function(): undefined} */
    MaterialSelectfield.prototype.check = MaterialSelectfield.prototype.check;
    /**
     * @return {undefined}
     */
    MaterialSelectfield.prototype.uncheck = function() {
      /** @type {boolean} */
      this.inputElement_.checked = false;
      this.updateClasses_();
    };
    /** @type {function(): undefined} */
    MaterialSelectfield.prototype.uncheck = MaterialSelectfield.prototype.uncheck;
    /**
     * @return {undefined}
     */
    MaterialSelectfield.prototype.init = function() {
      if (this.element_) {
        this.inputElement_ = this.element_.querySelector("." + this.CssClasses_.INPUT);
        /** @type {!Element} */
        var div = document.createElement("span");
        div.classList.add(this.CssClasses_.BOX_OUTLINE);
        /** @type {!Element} */
        var innerCircle = document.createElement("span");
        innerCircle.classList.add(this.CssClasses_.FOCUS_HELPER);
        /** @type {!Element} */
        var s = document.createElement("span");
        if (s.classList.add(this.CssClasses_.TICK_OUTLINE), div.appendChild(s), this.element_.appendChild(innerCircle), this.element_.appendChild(div), this.element_.classList.contains(this.CssClasses_.RIPPLE_EFFECT)) {
          this.element_.classList.add(this.CssClasses_.RIPPLE_IGNORE_EVENTS);
          /** @type {!Element} */
          this.rippleContainerElement_ = document.createElement("span");
          this.rippleContainerElement_.classList.add(this.CssClasses_.RIPPLE_CONTAINER);
          this.rippleContainerElement_.classList.add(this.CssClasses_.RIPPLE_EFFECT);
          this.rippleContainerElement_.classList.add(this.CssClasses_.RIPPLE_CENTER);
          this.boundRippleMouseUp = this.onMouseUp_.bind(this);
          this.rippleContainerElement_.addEventListener("mouseup", this.boundRippleMouseUp);
          /** @type {!Element} */
          var ripple = document.createElement("span");
          ripple.classList.add(this.CssClasses_.RIPPLE);
          this.rippleContainerElement_.appendChild(ripple);
          this.element_.appendChild(this.rippleContainerElement_);
        }
        this.boundInputOnChange = this.onChange_.bind(this);
        this.boundInputOnFocus = this.onFocus_.bind(this);
        this.boundInputOnBlur = this.onBlur_.bind(this);
        this.boundElementMouseUp = this.onMouseUp_.bind(this);
        this.inputElement_.addEventListener("change", this.boundInputOnChange);
        this.inputElement_.addEventListener("focus", this.boundInputOnFocus);
        this.inputElement_.addEventListener("blur", this.boundInputOnBlur);
        this.element_.addEventListener("mouseup", this.boundElementMouseUp);
        this.updateClasses_();
        this.element_.classList.add(this.CssClasses_.IS_UPGRADED);
      }
    };
    self.register({
      constructor : MaterialSelectfield,
      classAsString : "MaterialCheckbox",
      cssClass : "mdl-js-checkbox",
      widget : true
    });
    /**
     * @param {!Element} element
     * @return {undefined}
     */
    var Checkbox = function(element) {
      /** @type {!Element} */
      this.element_ = element;
      this.init();
    };
    /** @type {function(!Element): undefined} */
    window.MaterialIconToggle = Checkbox;
    Checkbox.prototype.Constant_ = {
      TINY_TIMEOUT : .001
    };
    Checkbox.prototype.CssClasses_ = {
      INPUT : "mdl-icon-toggle__input",
      JS_RIPPLE_EFFECT : "mdl-js-ripple-effect",
      RIPPLE_IGNORE_EVENTS : "mdl-js-ripple-effect--ignore-events",
      RIPPLE_CONTAINER : "mdl-icon-toggle__ripple-container",
      RIPPLE_CENTER : "mdl-ripple--center",
      RIPPLE : "mdl-ripple",
      IS_FOCUSED : "is-focused",
      IS_DISABLED : "is-disabled",
      IS_CHECKED : "is-checked"
    };
    /**
     * @param {!Object} delta
     * @return {undefined}
     */
    Checkbox.prototype.onChange_ = function(delta) {
      this.updateClasses_();
    };
    /**
     * @param {?} event
     * @return {undefined}
     */
    Checkbox.prototype.onFocus_ = function(event) {
      this.element_.classList.add(this.CssClasses_.IS_FOCUSED);
    };
    /**
     * @param {?} opt_e
     * @return {undefined}
     */
    Checkbox.prototype.onBlur_ = function(opt_e) {
      this.element_.classList.remove(this.CssClasses_.IS_FOCUSED);
    };
    /**
     * @param {?} event
     * @return {undefined}
     */
    Checkbox.prototype.onMouseUp_ = function(event) {
      this.blur_();
    };
    /**
     * @return {undefined}
     */
    Checkbox.prototype.updateClasses_ = function() {
      this.checkDisabled();
      this.checkToggleState();
    };
    /**
     * @return {undefined}
     */
    Checkbox.prototype.blur_ = function() {
      window.setTimeout(function() {
        this.inputElement_.blur();
      }.bind(this), this.Constant_.TINY_TIMEOUT);
    };
    /**
     * @return {undefined}
     */
    Checkbox.prototype.checkToggleState = function() {
      if (this.inputElement_.checked) {
        this.element_.classList.add(this.CssClasses_.IS_CHECKED);
      } else {
        this.element_.classList.remove(this.CssClasses_.IS_CHECKED);
      }
    };
    /** @type {function(): undefined} */
    Checkbox.prototype.checkToggleState = Checkbox.prototype.checkToggleState;
    /**
     * @return {undefined}
     */
    Checkbox.prototype.checkDisabled = function() {
      if (this.inputElement_.disabled) {
        this.element_.classList.add(this.CssClasses_.IS_DISABLED);
      } else {
        this.element_.classList.remove(this.CssClasses_.IS_DISABLED);
      }
    };
    /** @type {function(): undefined} */
    Checkbox.prototype.checkDisabled = Checkbox.prototype.checkDisabled;
    /**
     * @return {undefined}
     */
    Checkbox.prototype.disable = function() {
      /** @type {boolean} */
      this.inputElement_.disabled = true;
      this.updateClasses_();
    };
    /** @type {function(): undefined} */
    Checkbox.prototype.disable = Checkbox.prototype.disable;
    /**
     * @return {undefined}
     */
    Checkbox.prototype.enable = function() {
      /** @type {boolean} */
      this.inputElement_.disabled = false;
      this.updateClasses_();
    };
    /** @type {function(): undefined} */
    Checkbox.prototype.enable = Checkbox.prototype.enable;
    /**
     * @return {undefined}
     */
    Checkbox.prototype.check = function() {
      /** @type {boolean} */
      this.inputElement_.checked = true;
      this.updateClasses_();
    };
    /** @type {function(): undefined} */
    Checkbox.prototype.check = Checkbox.prototype.check;
    /**
     * @return {undefined}
     */
    Checkbox.prototype.uncheck = function() {
      /** @type {boolean} */
      this.inputElement_.checked = false;
      this.updateClasses_();
    };
    /** @type {function(): undefined} */
    Checkbox.prototype.uncheck = Checkbox.prototype.uncheck;
    /**
     * @return {undefined}
     */
    Checkbox.prototype.init = function() {
      if (this.element_) {
        if (this.inputElement_ = this.element_.querySelector("." + this.CssClasses_.INPUT), this.element_.classList.contains(this.CssClasses_.JS_RIPPLE_EFFECT)) {
          this.element_.classList.add(this.CssClasses_.RIPPLE_IGNORE_EVENTS);
          /** @type {!Element} */
          this.rippleContainerElement_ = document.createElement("span");
          this.rippleContainerElement_.classList.add(this.CssClasses_.RIPPLE_CONTAINER);
          this.rippleContainerElement_.classList.add(this.CssClasses_.JS_RIPPLE_EFFECT);
          this.rippleContainerElement_.classList.add(this.CssClasses_.RIPPLE_CENTER);
          this.boundRippleMouseUp = this.onMouseUp_.bind(this);
          this.rippleContainerElement_.addEventListener("mouseup", this.boundRippleMouseUp);
          /** @type {!Element} */
          var ripple = document.createElement("span");
          ripple.classList.add(this.CssClasses_.RIPPLE);
          this.rippleContainerElement_.appendChild(ripple);
          this.element_.appendChild(this.rippleContainerElement_);
        }
        this.boundInputOnChange = this.onChange_.bind(this);
        this.boundInputOnFocus = this.onFocus_.bind(this);
        this.boundInputOnBlur = this.onBlur_.bind(this);
        this.boundElementOnMouseUp = this.onMouseUp_.bind(this);
        this.inputElement_.addEventListener("change", this.boundInputOnChange);
        this.inputElement_.addEventListener("focus", this.boundInputOnFocus);
        this.inputElement_.addEventListener("blur", this.boundInputOnBlur);
        this.element_.addEventListener("mouseup", this.boundElementOnMouseUp);
        this.updateClasses_();
        this.element_.classList.add("is-upgraded");
      }
    };
    self.register({
      constructor : Checkbox,
      classAsString : "MaterialIconToggle",
      cssClass : "mdl-js-icon-toggle",
      widget : true
    });
    /**
     * @param {!HTMLElement} element
     * @return {undefined}
     */
    var MaterialStepper = function(element) {
      /** @type {!HTMLElement} */
      this.element_ = element;
      this.init();
    };
    /** @type {function(!HTMLElement): undefined} */
    window.MaterialMenu = MaterialStepper;
    MaterialStepper.prototype.Constant_ = {
      TRANSITION_DURATION_SECONDS : .3,
      TRANSITION_DURATION_FRACTION : .8,
      CLOSE_TIMEOUT : 150
    };
    MaterialStepper.prototype.Keycodes_ = {
      ENTER : 13,
      ESCAPE : 27,
      SPACE : 32,
      UP_ARROW : 38,
      DOWN_ARROW : 40
    };
    MaterialStepper.prototype.CssClasses_ = {
      CONTAINER : "mdl-menu__container",
      OUTLINE : "mdl-menu__outline",
      ITEM : "mdl-menu__item",
      ITEM_RIPPLE_CONTAINER : "mdl-menu__item-ripple-container",
      RIPPLE_EFFECT : "mdl-js-ripple-effect",
      RIPPLE_IGNORE_EVENTS : "mdl-js-ripple-effect--ignore-events",
      RIPPLE : "mdl-ripple",
      IS_UPGRADED : "is-upgraded",
      IS_VISIBLE : "is-visible",
      IS_ANIMATING : "is-animating",
      BOTTOM_LEFT : "mdl-menu--bottom-left",
      BOTTOM_RIGHT : "mdl-menu--bottom-right",
      TOP_LEFT : "mdl-menu--top-left",
      TOP_RIGHT : "mdl-menu--top-right",
      UNALIGNED : "mdl-menu--unaligned"
    };
    /**
     * @return {undefined}
     */
    MaterialStepper.prototype.init = function() {
      if (this.element_) {
        /** @type {!Element} */
        var container = document.createElement("div");
        container.classList.add(this.CssClasses_.CONTAINER);
        this.element_.parentElement.insertBefore(container, this.element_);
        this.element_.parentElement.removeChild(this.element_);
        container.appendChild(this.element_);
        /** @type {!Element} */
        this.container_ = container;
        /** @type {!Element} */
        var outline = document.createElement("div");
        outline.classList.add(this.CssClasses_.OUTLINE);
        /** @type {!Element} */
        this.outline_ = outline;
        container.insertBefore(outline, this.element_);
        var forElId = this.element_.getAttribute("for") || this.element_.getAttribute("data-mdl-for");
        /** @type {null} */
        var forEl = null;
        if (forElId && (forEl = document.getElementById(forElId))) {
          /** @type {!Element} */
          this.forElement_ = forEl;
          forEl.addEventListener("click", this.handleForClick_.bind(this));
          forEl.addEventListener("keydown", this.handleForKeyboardEvent_.bind(this));
        }
        var items = this.element_.querySelectorAll("." + this.CssClasses_.ITEM);
        this.boundItemKeydown_ = this.handleItemKeyboardEvent_.bind(this);
        this.boundItemClick_ = this.handleItemClick_.bind(this);
        /** @type {number} */
        var i = 0;
        for (; i < items.length; i++) {
          items[i].addEventListener("click", this.boundItemClick_);
          /** @type {string} */
          items[i].tabIndex = "-1";
          items[i].addEventListener("keydown", this.boundItemKeydown_);
        }
        if (this.element_.classList.contains(this.CssClasses_.RIPPLE_EFFECT)) {
          this.element_.classList.add(this.CssClasses_.RIPPLE_IGNORE_EVENTS);
          /** @type {number} */
          i = 0;
          for (; i < items.length; i++) {
            var w = items[i];
            /** @type {!Element} */
            var o = document.createElement("span");
            o.classList.add(this.CssClasses_.ITEM_RIPPLE_CONTAINER);
            /** @type {!Element} */
            var r = document.createElement("span");
            r.classList.add(this.CssClasses_.RIPPLE);
            o.appendChild(r);
            w.appendChild(o);
            w.classList.add(this.CssClasses_.RIPPLE_EFFECT);
          }
        }
        if (this.element_.classList.contains(this.CssClasses_.BOTTOM_LEFT)) {
          this.outline_.classList.add(this.CssClasses_.BOTTOM_LEFT);
        }
        if (this.element_.classList.contains(this.CssClasses_.BOTTOM_RIGHT)) {
          this.outline_.classList.add(this.CssClasses_.BOTTOM_RIGHT);
        }
        if (this.element_.classList.contains(this.CssClasses_.TOP_LEFT)) {
          this.outline_.classList.add(this.CssClasses_.TOP_LEFT);
        }
        if (this.element_.classList.contains(this.CssClasses_.TOP_RIGHT)) {
          this.outline_.classList.add(this.CssClasses_.TOP_RIGHT);
        }
        if (this.element_.classList.contains(this.CssClasses_.UNALIGNED)) {
          this.outline_.classList.add(this.CssClasses_.UNALIGNED);
        }
        container.classList.add(this.CssClasses_.IS_UPGRADED);
      }
    };
    /**
     * @param {!Function} evt
     * @return {undefined}
     */
    MaterialStepper.prototype.handleForClick_ = function(evt) {
      if (this.element_ && this.forElement_) {
        var properties = this.forElement_.getBoundingClientRect();
        var s = this.forElement_.parentElement.getBoundingClientRect();
        if (!this.element_.classList.contains(this.CssClasses_.UNALIGNED)) {
          if (this.element_.classList.contains(this.CssClasses_.BOTTOM_RIGHT)) {
            /** @type {string} */
            this.container_.style.right = s.right - properties.right + "px";
            /** @type {string} */
            this.container_.style.top = this.forElement_.offsetTop + this.forElement_.offsetHeight + "px";
          } else {
            if (this.element_.classList.contains(this.CssClasses_.TOP_LEFT)) {
              /** @type {string} */
              this.container_.style.left = this.forElement_.offsetLeft + "px";
              /** @type {string} */
              this.container_.style.bottom = s.bottom - properties.top + "px";
            } else {
              if (this.element_.classList.contains(this.CssClasses_.TOP_RIGHT)) {
                /** @type {string} */
                this.container_.style.right = s.right - properties.right + "px";
                /** @type {string} */
                this.container_.style.bottom = s.bottom - properties.top + "px";
              } else {
                /** @type {string} */
                this.container_.style.left = this.forElement_.offsetLeft + "px";
                /** @type {string} */
                this.container_.style.top = this.forElement_.offsetTop + this.forElement_.offsetHeight + "px";
              }
            }
          }
        }
      }
      this.toggle(evt);
    };
    /**
     * @param {!Event} evt
     * @return {undefined}
     */
    MaterialStepper.prototype.handleForKeyboardEvent_ = function(evt) {
      if (this.element_ && this.container_ && this.forElement_) {
        var $aufocused = this.element_.querySelectorAll("." + this.CssClasses_.ITEM + ":not([disabled])");
        if ($aufocused && $aufocused.length > 0 && this.container_.classList.contains(this.CssClasses_.IS_VISIBLE)) {
          if (evt.keyCode === this.Keycodes_.UP_ARROW) {
            evt.preventDefault();
            $aufocused[$aufocused.length - 1].focus();
          } else {
            if (evt.keyCode === this.Keycodes_.DOWN_ARROW) {
              evt.preventDefault();
              $aufocused[0].focus();
            }
          }
        }
      }
    };
    /**
     * @param {!Event} evt
     * @return {undefined}
     */
    MaterialStepper.prototype.handleItemKeyboardEvent_ = function(evt) {
      if (this.element_ && this.container_) {
        var items = this.element_.querySelectorAll("." + this.CssClasses_.ITEM + ":not([disabled])");
        if (items && items.length > 0 && this.container_.classList.contains(this.CssClasses_.IS_VISIBLE)) {
          /** @type {number} */
          var currentIndex = Array.prototype.slice.call(items).indexOf(evt.target);
          if (evt.keyCode === this.Keycodes_.UP_ARROW) {
            evt.preventDefault();
            if (currentIndex > 0) {
              items[currentIndex - 1].focus();
            } else {
              items[items.length - 1].focus();
            }
          } else {
            if (evt.keyCode === this.Keycodes_.DOWN_ARROW) {
              evt.preventDefault();
              if (items.length > currentIndex + 1) {
                items[currentIndex + 1].focus();
              } else {
                items[0].focus();
              }
            } else {
              if (evt.keyCode === this.Keycodes_.SPACE || evt.keyCode === this.Keycodes_.ENTER) {
                evt.preventDefault();
                /** @type {!MouseEvent} */
                var e = new MouseEvent("mousedown");
                evt.target.dispatchEvent(e);
                /** @type {!MouseEvent} */
                e = new MouseEvent("mouseup");
                evt.target.dispatchEvent(e);
                evt.target.click();
              } else {
                if (evt.keyCode === this.Keycodes_.ESCAPE) {
                  evt.preventDefault();
                  this.hide();
                }
              }
            }
          }
        }
      }
    };
    /**
     * @param {!Event} evt
     * @return {undefined}
     */
    MaterialStepper.prototype.handleItemClick_ = function(evt) {
      if (evt.target.hasAttribute("disabled")) {
        evt.stopPropagation();
      } else {
        /** @type {boolean} */
        this.closing_ = true;
        window.setTimeout(function(canCreateDiscussions) {
          this.hide();
          /** @type {boolean} */
          this.closing_ = false;
        }.bind(this), this.Constant_.CLOSE_TIMEOUT);
      }
    };
    /**
     * @param {string} width
     * @param {string} height
     * @return {undefined}
     */
    MaterialStepper.prototype.applyClip_ = function(width, height) {
      if (this.element_.classList.contains(this.CssClasses_.UNALIGNED)) {
        /** @type {string} */
        this.element_.style.clip = "";
      } else {
        if (this.element_.classList.contains(this.CssClasses_.BOTTOM_RIGHT)) {
          /** @type {string} */
          this.element_.style.clip = "rect(0 " + height + "px 0 " + height + "px)";
        } else {
          if (this.element_.classList.contains(this.CssClasses_.TOP_LEFT)) {
            /** @type {string} */
            this.element_.style.clip = "rect(" + width + "px 0 " + width + "px 0)";
          } else {
            if (this.element_.classList.contains(this.CssClasses_.TOP_RIGHT)) {
              /** @type {string} */
              this.element_.style.clip = "rect(" + width + "px " + height + "px " + width + "px " + height + "px)";
            } else {
              /** @type {string} */
              this.element_.style.clip = "";
            }
          }
        }
      }
    };
    /**
     * @param {!Event} treeOutline
     * @return {undefined}
     */
    MaterialStepper.prototype.removeAnimationEndListener_ = function(treeOutline) {
      treeOutline.target.classList.remove(MaterialStepper.prototype.CssClasses_.IS_ANIMATING);
    };
    /**
     * @return {undefined}
     */
    MaterialStepper.prototype.addAnimationEndListener_ = function() {
      this.element_.addEventListener("transitionend", this.removeAnimationEndListener_);
      this.element_.addEventListener("webkitTransitionEnd", this.removeAnimationEndListener_);
    };
    /**
     * @param {!Function} e
     * @return {undefined}
     */
    MaterialStepper.prototype.show = function(e) {
      if (this.element_ && this.container_ && this.outline_) {
        var height = this.element_.getBoundingClientRect().height;
        var width = this.element_.getBoundingClientRect().width;
        /** @type {string} */
        this.container_.style.width = width + "px";
        /** @type {string} */
        this.container_.style.height = height + "px";
        /** @type {string} */
        this.outline_.style.width = width + "px";
        /** @type {string} */
        this.outline_.style.height = height + "px";
        /** @type {number} */
        var transitionDuration = this.Constant_.TRANSITION_DURATION_SECONDS * this.Constant_.TRANSITION_DURATION_FRACTION;
        var lines = this.element_.querySelectorAll("." + this.CssClasses_.ITEM);
        /** @type {number} */
        var i = 0;
        for (; i < lines.length; i++) {
          /** @type {null} */
          var zero = null;
          /** @type {string} */
          zero = this.element_.classList.contains(this.CssClasses_.TOP_LEFT) || this.element_.classList.contains(this.CssClasses_.TOP_RIGHT) ? (height - lines[i].offsetTop - lines[i].offsetHeight) / height * transitionDuration + "s" : lines[i].offsetTop / height * transitionDuration + "s";
          /** @type {string} */
          lines[i].style.transitionDelay = zero;
        }
        this.applyClip_(height, width);
        window.requestAnimationFrame(function() {
          this.element_.classList.add(this.CssClasses_.IS_ANIMATING);
          /** @type {string} */
          this.element_.style.clip = "rect(0 " + width + "px " + height + "px 0)";
          this.container_.classList.add(this.CssClasses_.IS_VISIBLE);
        }.bind(this));
        this.addAnimationEndListener_();
        var resizeOutput = function(n) {
          if (!(n === e || this.closing_ || n.target.parentNode === this.element_)) {
            document.removeEventListener("click", resizeOutput);
            this.hide();
          }
        }.bind(this);
        document.addEventListener("click", resizeOutput);
      }
    };
    /** @type {function(!Function): undefined} */
    MaterialStepper.prototype.show = MaterialStepper.prototype.show;
    /**
     * @return {undefined}
     */
    MaterialStepper.prototype.hide = function() {
      if (this.element_ && this.container_ && this.outline_) {
        var addedImgs = this.element_.querySelectorAll("." + this.CssClasses_.ITEM);
        /** @type {number} */
        var i = 0;
        for (; i < addedImgs.length; i++) {
          addedImgs[i].style.removeProperty("transition-delay");
        }
        var _entity$getData = this.element_.getBoundingClientRect();
        var height = _entity$getData.height;
        var width = _entity$getData.width;
        this.element_.classList.add(this.CssClasses_.IS_ANIMATING);
        this.applyClip_(height, width);
        this.container_.classList.remove(this.CssClasses_.IS_VISIBLE);
        this.addAnimationEndListener_();
      }
    };
    /** @type {function(): undefined} */
    MaterialStepper.prototype.hide = MaterialStepper.prototype.hide;
    /**
     * @param {!Function} e
     * @return {undefined}
     */
    MaterialStepper.prototype.toggle = function(e) {
      if (this.container_.classList.contains(this.CssClasses_.IS_VISIBLE)) {
        this.hide();
      } else {
        this.show(e);
      }
    };
    /** @type {function(!Function): undefined} */
    MaterialStepper.prototype.toggle = MaterialStepper.prototype.toggle;
    self.register({
      constructor : MaterialStepper,
      classAsString : "MaterialMenu",
      cssClass : "mdl-js-menu",
      widget : true
    });
    /**
     * @param {!Element} elem
     * @return {undefined}
     */
    var Player = function(elem) {
      /** @type {!Element} */
      this.element_ = elem;
      this.init();
    };
    /** @type {function(!Element): undefined} */
    window.MaterialProgress = Player;
    Player.prototype.Constant_ = {};
    Player.prototype.CssClasses_ = {
      INDETERMINATE_CLASS : "mdl-progress__indeterminate"
    };
    /**
     * @param {string} percent
     * @return {undefined}
     */
    Player.prototype.setProgress = function(percent) {
      if (!this.element_.classList.contains(this.CssClasses_.INDETERMINATE_CLASS)) {
        /** @type {string} */
        this.progressbar_.style.width = percent + "%";
      }
    };
    /** @type {function(string): undefined} */
    Player.prototype.setProgress = Player.prototype.setProgress;
    /**
     * @param {number} val
     * @return {undefined}
     */
    Player.prototype.setBuffer = function(val) {
      /** @type {string} */
      this.bufferbar_.style.width = val + "%";
      /** @type {string} */
      this.auxbar_.style.width = 100 - val + "%";
    };
    /** @type {function(number): undefined} */
    Player.prototype.setBuffer = Player.prototype.setBuffer;
    /**
     * @return {undefined}
     */
    Player.prototype.init = function() {
      if (this.element_) {
        /** @type {!Element} */
        var el = document.createElement("div");
        /** @type {string} */
        el.className = "progressbar bar bar1";
        this.element_.appendChild(el);
        /** @type {!Element} */
        this.progressbar_ = el;
        /** @type {string} */
        (el = document.createElement("div")).className = "bufferbar bar bar2";
        this.element_.appendChild(el);
        /** @type {!Element} */
        this.bufferbar_ = el;
        /** @type {string} */
        (el = document.createElement("div")).className = "auxbar bar bar3";
        this.element_.appendChild(el);
        /** @type {!Element} */
        this.auxbar_ = el;
        /** @type {string} */
        this.progressbar_.style.width = "0%";
        /** @type {string} */
        this.bufferbar_.style.width = "100%";
        /** @type {string} */
        this.auxbar_.style.width = "0%";
        this.element_.classList.add("is-upgraded");
      }
    };
    self.register({
      constructor : Player,
      classAsString : "MaterialProgress",
      cssClass : "mdl-js-progress",
      widget : true
    });
    /**
     * @param {!Element} elem
     * @return {undefined}
     */
    var $ = function(elem) {
      /** @type {!Element} */
      this.element_ = elem;
      this.init();
    };
    /** @type {function(!Element): undefined} */
    window.MaterialRadio = $;
    $.prototype.Constant_ = {
      TINY_TIMEOUT : .001
    };
    $.prototype.CssClasses_ = {
      IS_FOCUSED : "is-focused",
      IS_DISABLED : "is-disabled",
      IS_CHECKED : "is-checked",
      IS_UPGRADED : "is-upgraded",
      JS_RADIO : "mdl-js-radio",
      RADIO_BTN : "mdl-radio__button",
      RADIO_OUTER_CIRCLE : "mdl-radio__outer-circle",
      RADIO_INNER_CIRCLE : "mdl-radio__inner-circle",
      RIPPLE_EFFECT : "mdl-js-ripple-effect",
      RIPPLE_IGNORE_EVENTS : "mdl-js-ripple-effect--ignore-events",
      RIPPLE_CONTAINER : "mdl-radio__ripple-container",
      RIPPLE_CENTER : "mdl-ripple--center",
      RIPPLE : "mdl-ripple"
    };
    /**
     * @param {!Object} delta
     * @return {undefined}
     */
    $.prototype.onChange_ = function(delta) {
      /** @type {!NodeList<Element>} */
      var songTimeVisualizations = document.getElementsByClassName(this.CssClasses_.JS_RADIO);
      /** @type {number} */
      var i = 0;
      for (; i < songTimeVisualizations.length; i++) {
        if (songTimeVisualizations[i].querySelector("." + this.CssClasses_.RADIO_BTN).getAttribute("name") === this.btnElement_.getAttribute("name") && void 0 !== songTimeVisualizations[i].MaterialRadio) {
          songTimeVisualizations[i].MaterialRadio.updateClasses_();
        }
      }
    };
    /**
     * @param {?} event
     * @return {undefined}
     */
    $.prototype.onFocus_ = function(event) {
      this.element_.classList.add(this.CssClasses_.IS_FOCUSED);
    };
    /**
     * @param {?} opt_e
     * @return {undefined}
     */
    $.prototype.onBlur_ = function(opt_e) {
      this.element_.classList.remove(this.CssClasses_.IS_FOCUSED);
    };
    /**
     * @param {?} event
     * @return {undefined}
     */
    $.prototype.onMouseup_ = function(event) {
      this.blur_();
    };
    /**
     * @return {undefined}
     */
    $.prototype.updateClasses_ = function() {
      this.checkDisabled();
      this.checkToggleState();
    };
    /**
     * @return {undefined}
     */
    $.prototype.blur_ = function() {
      window.setTimeout(function() {
        this.btnElement_.blur();
      }.bind(this), this.Constant_.TINY_TIMEOUT);
    };
    /**
     * @return {undefined}
     */
    $.prototype.checkDisabled = function() {
      if (this.btnElement_.disabled) {
        this.element_.classList.add(this.CssClasses_.IS_DISABLED);
      } else {
        this.element_.classList.remove(this.CssClasses_.IS_DISABLED);
      }
    };
    /** @type {function(): undefined} */
    $.prototype.checkDisabled = $.prototype.checkDisabled;
    /**
     * @return {undefined}
     */
    $.prototype.checkToggleState = function() {
      if (this.btnElement_.checked) {
        this.element_.classList.add(this.CssClasses_.IS_CHECKED);
      } else {
        this.element_.classList.remove(this.CssClasses_.IS_CHECKED);
      }
    };
    /** @type {function(): undefined} */
    $.prototype.checkToggleState = $.prototype.checkToggleState;
    /**
     * @return {undefined}
     */
    $.prototype.disable = function() {
      /** @type {boolean} */
      this.btnElement_.disabled = true;
      this.updateClasses_();
    };
    /** @type {function(): undefined} */
    $.prototype.disable = $.prototype.disable;
    /**
     * @return {undefined}
     */
    $.prototype.enable = function() {
      /** @type {boolean} */
      this.btnElement_.disabled = false;
      this.updateClasses_();
    };
    /** @type {function(): undefined} */
    $.prototype.enable = $.prototype.enable;
    /**
     * @return {undefined}
     */
    $.prototype.check = function() {
      /** @type {boolean} */
      this.btnElement_.checked = true;
      this.onChange_(null);
    };
    /** @type {function(): undefined} */
    $.prototype.check = $.prototype.check;
    /**
     * @return {undefined}
     */
    $.prototype.uncheck = function() {
      /** @type {boolean} */
      this.btnElement_.checked = false;
      this.onChange_(null);
    };
    /** @type {function(): undefined} */
    $.prototype.uncheck = $.prototype.uncheck;
    /**
     * @return {undefined}
     */
    $.prototype.init = function() {
      if (this.element_) {
        this.btnElement_ = this.element_.querySelector("." + this.CssClasses_.RADIO_BTN);
        this.boundChangeHandler_ = this.onChange_.bind(this);
        this.boundFocusHandler_ = this.onChange_.bind(this);
        this.boundBlurHandler_ = this.onBlur_.bind(this);
        this.boundMouseUpHandler_ = this.onMouseup_.bind(this);
        /** @type {!Element} */
        var innerCircle = document.createElement("span");
        innerCircle.classList.add(this.CssClasses_.RADIO_OUTER_CIRCLE);
        /** @type {!Element} */
        var tickContainer = document.createElement("span");
        tickContainer.classList.add(this.CssClasses_.RADIO_INNER_CIRCLE);
        this.element_.appendChild(innerCircle);
        this.element_.appendChild(tickContainer);
        var div;
        if (this.element_.classList.contains(this.CssClasses_.RIPPLE_EFFECT)) {
          this.element_.classList.add(this.CssClasses_.RIPPLE_IGNORE_EVENTS);
          (div = document.createElement("span")).classList.add(this.CssClasses_.RIPPLE_CONTAINER);
          div.classList.add(this.CssClasses_.RIPPLE_EFFECT);
          div.classList.add(this.CssClasses_.RIPPLE_CENTER);
          div.addEventListener("mouseup", this.boundMouseUpHandler_);
          /** @type {!Element} */
          var i = document.createElement("span");
          i.classList.add(this.CssClasses_.RIPPLE);
          div.appendChild(i);
          this.element_.appendChild(div);
        }
        this.btnElement_.addEventListener("change", this.boundChangeHandler_);
        this.btnElement_.addEventListener("focus", this.boundFocusHandler_);
        this.btnElement_.addEventListener("blur", this.boundBlurHandler_);
        this.element_.addEventListener("mouseup", this.boundMouseUpHandler_);
        this.updateClasses_();
        this.element_.classList.add(this.CssClasses_.IS_UPGRADED);
      }
    };
    self.register({
      constructor : $,
      classAsString : "MaterialRadio",
      cssClass : "mdl-js-radio",
      widget : true
    });
    /**
     * @param {!Element} el
     * @return {undefined}
     */
    var Editor = function(el) {
      /** @type {!Element} */
      this.element_ = el;
      /** @type {boolean} */
      this.isIE_ = window.navigator.msPointerEnabled;
      this.init();
    };
    /** @type {function(!Element): undefined} */
    window.MaterialSlider = Editor;
    Editor.prototype.Constant_ = {};
    Editor.prototype.CssClasses_ = {
      IE_CONTAINER : "mdl-slider__ie-container",
      SLIDER_CONTAINER : "mdl-slider__container",
      BACKGROUND_FLEX : "mdl-slider__background-flex",
      BACKGROUND_LOWER : "mdl-slider__background-lower",
      BACKGROUND_UPPER : "mdl-slider__background-upper",
      IS_LOWEST_VALUE : "is-lowest-value",
      IS_UPGRADED : "is-upgraded"
    };
    /**
     * @param {?} event
     * @return {undefined}
     */
    Editor.prototype.onInput_ = function(event) {
      this.updateValueStyles_();
    };
    /**
     * @param {!Object} delta
     * @return {undefined}
     */
    Editor.prototype.onChange_ = function(delta) {
      this.updateValueStyles_();
    };
    /**
     * @param {!Event} event
     * @return {undefined}
     */
    Editor.prototype.onMouseUp_ = function(event) {
      event.target.blur();
    };
    /**
     * @param {!Event} event
     * @return {undefined}
     */
    Editor.prototype.onContainerMouseDown_ = function(event) {
      if (event.target === this.element_.parentElement) {
        event.preventDefault();
        /** @type {!MouseEvent} */
        var newEvent = new MouseEvent("mousedown", {
          target : event.target,
          buttons : event.buttons,
          clientX : event.clientX,
          clientY : this.element_.getBoundingClientRect().y
        });
        this.element_.dispatchEvent(newEvent);
      }
    };
    /**
     * @return {undefined}
     */
    Editor.prototype.updateValueStyles_ = function() {
      /** @type {number} */
      var fraction = (this.element_.value - this.element_.min) / (this.element_.max - this.element_.min);
      if (0 === fraction) {
        this.element_.classList.add(this.CssClasses_.IS_LOWEST_VALUE);
      } else {
        this.element_.classList.remove(this.CssClasses_.IS_LOWEST_VALUE);
      }
      if (!this.isIE_) {
        /** @type {number} */
        this.backgroundLower_.style.flex = fraction;
        /** @type {number} */
        this.backgroundLower_.style.webkitFlex = fraction;
        /** @type {number} */
        this.backgroundUpper_.style.flex = 1 - fraction;
        /** @type {number} */
        this.backgroundUpper_.style.webkitFlex = 1 - fraction;
      }
    };
    /**
     * @return {undefined}
     */
    Editor.prototype.disable = function() {
      /** @type {boolean} */
      this.element_.disabled = true;
    };
    /** @type {function(): undefined} */
    Editor.prototype.disable = Editor.prototype.disable;
    /**
     * @return {undefined}
     */
    Editor.prototype.enable = function() {
      /** @type {boolean} */
      this.element_.disabled = false;
    };
    /** @type {function(): undefined} */
    Editor.prototype.enable = Editor.prototype.enable;
    /**
     * @param {string} value
     * @return {undefined}
     */
    Editor.prototype.change = function(value) {
      if (void 0 !== value) {
        /** @type {string} */
        this.element_.value = value;
      }
      this.updateValueStyles_();
    };
    /** @type {function(string): undefined} */
    Editor.prototype.change = Editor.prototype.change;
    /**
     * @return {undefined}
     */
    Editor.prototype.init = function() {
      if (this.element_) {
        if (this.isIE_) {
          /** @type {!Element} */
          var container = document.createElement("div");
          container.classList.add(this.CssClasses_.IE_CONTAINER);
          this.element_.parentElement.insertBefore(container, this.element_);
          this.element_.parentElement.removeChild(this.element_);
          container.appendChild(this.element_);
        } else {
          /** @type {!Element} */
          var container = document.createElement("div");
          container.classList.add(this.CssClasses_.SLIDER_CONTAINER);
          this.element_.parentElement.insertBefore(container, this.element_);
          this.element_.parentElement.removeChild(this.element_);
          container.appendChild(this.element_);
          /** @type {!Element} */
          var backgroundFlex = document.createElement("div");
          backgroundFlex.classList.add(this.CssClasses_.BACKGROUND_FLEX);
          container.appendChild(backgroundFlex);
          /** @type {!Element} */
          this.backgroundLower_ = document.createElement("div");
          this.backgroundLower_.classList.add(this.CssClasses_.BACKGROUND_LOWER);
          backgroundFlex.appendChild(this.backgroundLower_);
          /** @type {!Element} */
          this.backgroundUpper_ = document.createElement("div");
          this.backgroundUpper_.classList.add(this.CssClasses_.BACKGROUND_UPPER);
          backgroundFlex.appendChild(this.backgroundUpper_);
        }
        this.boundInputHandler = this.onInput_.bind(this);
        this.boundChangeHandler = this.onChange_.bind(this);
        this.boundMouseUpHandler = this.onMouseUp_.bind(this);
        this.boundContainerMouseDownHandler = this.onContainerMouseDown_.bind(this);
        this.element_.addEventListener("input", this.boundInputHandler);
        this.element_.addEventListener("change", this.boundChangeHandler);
        this.element_.addEventListener("mouseup", this.boundMouseUpHandler);
        this.element_.parentElement.addEventListener("mousedown", this.boundContainerMouseDownHandler);
        this.updateValueStyles_();
        this.element_.classList.add(this.CssClasses_.IS_UPGRADED);
      }
    };
    self.register({
      constructor : Editor,
      classAsString : "MaterialSlider",
      cssClass : "mdl-js-slider",
      widget : true
    });
    /**
     * @param {!HTMLElement} elem
     * @return {undefined}
     */
    var handler = function(elem) {
      if (this.element_ = elem, this.textElement_ = this.element_.querySelector("." + this.cssClasses_.MESSAGE), this.actionElement_ = this.element_.querySelector("." + this.cssClasses_.ACTION), !this.textElement_) {
        throw new Error("There must be a message element for a snackbar.");
      }
      if (!this.actionElement_) {
        throw new Error("There must be an action element for a snackbar.");
      }
      /** @type {boolean} */
      this.active = false;
      this.actionHandler_ = void 0;
      this.message_ = void 0;
      this.actionText_ = void 0;
      /** @type {!Array} */
      this.queuedNotifications_ = [];
      this.setActionHidden_(true);
    };
    /** @type {function(!HTMLElement): undefined} */
    window.MaterialSnackbar = handler;
    handler.prototype.Constant_ = {
      ANIMATION_LENGTH : 250
    };
    handler.prototype.cssClasses_ = {
      SNACKBAR : "mdl-snackbar",
      MESSAGE : "mdl-snackbar__text",
      ACTION : "mdl-snackbar__action",
      ACTIVE : "mdl-snackbar--active"
    };
    /**
     * @return {undefined}
     */
    handler.prototype.displaySnackbar_ = function() {
      this.element_.setAttribute("aria-hidden", "true");
      if (this.actionHandler_) {
        this.actionElement_.textContent = this.actionText_;
        this.actionElement_.addEventListener("click", this.actionHandler_);
        this.setActionHidden_(false);
      }
      this.textElement_.textContent = this.message_;
      this.element_.classList.add(this.cssClasses_.ACTIVE);
      this.element_.setAttribute("aria-hidden", "false");
      setTimeout(this.cleanup_.bind(this), this.timeout_);
    };
    /**
     * @param {!Object} data
     * @return {undefined}
     */
    handler.prototype.showSnackbar = function(data) {
      if (void 0 === data) {
        throw new Error("Please provide a data object with at least a message to display.");
      }
      if (void 0 === data.message) {
        throw new Error("Please provide a message to be displayed.");
      }
      if (data.actionHandler && !data.actionText) {
        throw new Error("Please provide action text with the handler.");
      }
      if (this.active) {
        this.queuedNotifications_.push(data);
      } else {
        /** @type {boolean} */
        this.active = true;
        this.message_ = data.message;
        if (data.timeout) {
          this.timeout_ = data.timeout;
        } else {
          /** @type {number} */
          this.timeout_ = 2750;
        }
        if (data.actionHandler) {
          this.actionHandler_ = data.actionHandler;
        }
        if (data.actionText) {
          this.actionText_ = data.actionText;
        }
        this.displaySnackbar_();
      }
    };
    /** @type {function(!Object): undefined} */
    handler.prototype.showSnackbar = handler.prototype.showSnackbar;
    /**
     * @return {undefined}
     */
    handler.prototype.checkQueue_ = function() {
      if (this.queuedNotifications_.length > 0) {
        this.showSnackbar(this.queuedNotifications_.shift());
      }
    };
    /**
     * @return {undefined}
     */
    handler.prototype.cleanup_ = function() {
      this.element_.classList.remove(this.cssClasses_.ACTIVE);
      setTimeout(function() {
        this.element_.setAttribute("aria-hidden", "true");
        /** @type {string} */
        this.textElement_.textContent = "";
        if (!Boolean(this.actionElement_.getAttribute("aria-hidden"))) {
          this.setActionHidden_(true);
          /** @type {string} */
          this.actionElement_.textContent = "";
          this.actionElement_.removeEventListener("click", this.actionHandler_);
        }
        this.actionHandler_ = void 0;
        this.message_ = void 0;
        this.actionText_ = void 0;
        /** @type {boolean} */
        this.active = false;
        this.checkQueue_();
      }.bind(this), this.Constant_.ANIMATION_LENGTH);
    };
    /**
     * @param {boolean} isHidden
     * @return {undefined}
     */
    handler.prototype.setActionHidden_ = function(isHidden) {
      if (isHidden) {
        this.actionElement_.setAttribute("aria-hidden", "true");
      } else {
        this.actionElement_.removeAttribute("aria-hidden");
      }
    };
    self.register({
      constructor : handler,
      classAsString : "MaterialSnackbar",
      cssClass : "mdl-js-snackbar",
      widget : true
    });
    /**
     * @param {!Element} element
     * @return {undefined}
     */
    var MaterialAvatar = function(element) {
      /** @type {!Element} */
      this.element_ = element;
      this.init();
    };
    /** @type {function(!Element): undefined} */
    window.MaterialSpinner = MaterialAvatar;
    MaterialAvatar.prototype.Constant_ = {
      MDL_SPINNER_LAYER_COUNT : 4
    };
    MaterialAvatar.prototype.CssClasses_ = {
      MDL_SPINNER_LAYER : "mdl-spinner__layer",
      MDL_SPINNER_CIRCLE_CLIPPER : "mdl-spinner__circle-clipper",
      MDL_SPINNER_CIRCLE : "mdl-spinner__circle",
      MDL_SPINNER_GAP_PATCH : "mdl-spinner__gap-patch",
      MDL_SPINNER_LEFT : "mdl-spinner__left",
      MDL_SPINNER_RIGHT : "mdl-spinner__right"
    };
    /**
     * @param {number} name
     * @return {undefined}
     */
    MaterialAvatar.prototype.createLayer = function(name) {
      /** @type {!Element} */
      var div = document.createElement("div");
      div.classList.add(this.CssClasses_.MDL_SPINNER_LAYER);
      div.classList.add(this.CssClasses_.MDL_SPINNER_LAYER + "-" + name);
      /** @type {!Element} */
      var border = document.createElement("div");
      border.classList.add(this.CssClasses_.MDL_SPINNER_CIRCLE_CLIPPER);
      border.classList.add(this.CssClasses_.MDL_SPINNER_LEFT);
      /** @type {!Element} */
      var i = document.createElement("div");
      i.classList.add(this.CssClasses_.MDL_SPINNER_GAP_PATCH);
      /** @type {!Element} */
      var n = document.createElement("div");
      n.classList.add(this.CssClasses_.MDL_SPINNER_CIRCLE_CLIPPER);
      n.classList.add(this.CssClasses_.MDL_SPINNER_RIGHT);
      /** @type {!Array} */
      var names = [border, i, n];
      /** @type {number} */
      var index = 0;
      for (; index < names.length; index++) {
        /** @type {!Element} */
        var bezierPreviewContainer = document.createElement("div");
        bezierPreviewContainer.classList.add(this.CssClasses_.MDL_SPINNER_CIRCLE);
        names[index].appendChild(bezierPreviewContainer);
      }
      div.appendChild(border);
      div.appendChild(i);
      div.appendChild(n);
      this.element_.appendChild(div);
    };
    /** @type {function(number): undefined} */
    MaterialAvatar.prototype.createLayer = MaterialAvatar.prototype.createLayer;
    /**
     * @return {undefined}
     */
    MaterialAvatar.prototype.stop = function() {
      this.element_.classList.remove("is-active");
    };
    /** @type {function(): undefined} */
    MaterialAvatar.prototype.stop = MaterialAvatar.prototype.stop;
    /**
     * @return {undefined}
     */
    MaterialAvatar.prototype.start = function() {
      this.element_.classList.add("is-active");
    };
    /** @type {function(): undefined} */
    MaterialAvatar.prototype.start = MaterialAvatar.prototype.start;
    /**
     * @return {undefined}
     */
    MaterialAvatar.prototype.init = function() {
      if (this.element_) {
        /** @type {number} */
        var i = 1;
        for (; i <= this.Constant_.MDL_SPINNER_LAYER_COUNT; i++) {
          this.createLayer(i);
        }
        this.element_.classList.add("is-upgraded");
      }
    };
    self.register({
      constructor : MaterialAvatar,
      classAsString : "MaterialSpinner",
      cssClass : "mdl-js-spinner",
      widget : true
    });
    /**
     * @param {!Element} val
     * @return {undefined}
     */
    var that = function(val) {
      /** @type {!Element} */
      this.element_ = val;
      this.init();
    };
    /** @type {function(!Element): undefined} */
    window.MaterialSwitch = that;
    that.prototype.Constant_ = {
      TINY_TIMEOUT : .001
    };
    that.prototype.CssClasses_ = {
      INPUT : "mdl-switch__input",
      TRACK : "mdl-switch__track",
      THUMB : "mdl-switch__thumb",
      FOCUS_HELPER : "mdl-switch__focus-helper",
      RIPPLE_EFFECT : "mdl-js-ripple-effect",
      RIPPLE_IGNORE_EVENTS : "mdl-js-ripple-effect--ignore-events",
      RIPPLE_CONTAINER : "mdl-switch__ripple-container",
      RIPPLE_CENTER : "mdl-ripple--center",
      RIPPLE : "mdl-ripple",
      IS_FOCUSED : "is-focused",
      IS_DISABLED : "is-disabled",
      IS_CHECKED : "is-checked"
    };
    /**
     * @param {!Object} delta
     * @return {undefined}
     */
    that.prototype.onChange_ = function(delta) {
      this.updateClasses_();
    };
    /**
     * @param {?} event
     * @return {undefined}
     */
    that.prototype.onFocus_ = function(event) {
      this.element_.classList.add(this.CssClasses_.IS_FOCUSED);
    };
    /**
     * @param {?} opt_e
     * @return {undefined}
     */
    that.prototype.onBlur_ = function(opt_e) {
      this.element_.classList.remove(this.CssClasses_.IS_FOCUSED);
    };
    /**
     * @param {?} event
     * @return {undefined}
     */
    that.prototype.onMouseUp_ = function(event) {
      this.blur_();
    };
    /**
     * @return {undefined}
     */
    that.prototype.updateClasses_ = function() {
      this.checkDisabled();
      this.checkToggleState();
    };
    /**
     * @return {undefined}
     */
    that.prototype.blur_ = function() {
      window.setTimeout(function() {
        this.inputElement_.blur();
      }.bind(this), this.Constant_.TINY_TIMEOUT);
    };
    /**
     * @return {undefined}
     */
    that.prototype.checkDisabled = function() {
      if (this.inputElement_.disabled) {
        this.element_.classList.add(this.CssClasses_.IS_DISABLED);
      } else {
        this.element_.classList.remove(this.CssClasses_.IS_DISABLED);
      }
    };
    /** @type {function(): undefined} */
    that.prototype.checkDisabled = that.prototype.checkDisabled;
    /**
     * @return {undefined}
     */
    that.prototype.checkToggleState = function() {
      if (this.inputElement_.checked) {
        this.element_.classList.add(this.CssClasses_.IS_CHECKED);
      } else {
        this.element_.classList.remove(this.CssClasses_.IS_CHECKED);
      }
    };
    /** @type {function(): undefined} */
    that.prototype.checkToggleState = that.prototype.checkToggleState;
    /**
     * @return {undefined}
     */
    that.prototype.disable = function() {
      /** @type {boolean} */
      this.inputElement_.disabled = true;
      this.updateClasses_();
    };
    /** @type {function(): undefined} */
    that.prototype.disable = that.prototype.disable;
    /**
     * @return {undefined}
     */
    that.prototype.enable = function() {
      /** @type {boolean} */
      this.inputElement_.disabled = false;
      this.updateClasses_();
    };
    /** @type {function(): undefined} */
    that.prototype.enable = that.prototype.enable;
    /**
     * @return {undefined}
     */
    that.prototype.on = function() {
      /** @type {boolean} */
      this.inputElement_.checked = true;
      this.updateClasses_();
    };
    /** @type {function(): undefined} */
    that.prototype.on = that.prototype.on;
    /**
     * @return {undefined}
     */
    that.prototype.off = function() {
      /** @type {boolean} */
      this.inputElement_.checked = false;
      this.updateClasses_();
    };
    /** @type {function(): undefined} */
    that.prototype.off = that.prototype.off;
    /**
     * @return {undefined}
     */
    that.prototype.init = function() {
      if (this.element_) {
        this.inputElement_ = this.element_.querySelector("." + this.CssClasses_.INPUT);
        /** @type {!Element} */
        var thumb = document.createElement("div");
        thumb.classList.add(this.CssClasses_.TRACK);
        /** @type {!Element} */
        var div = document.createElement("div");
        div.classList.add(this.CssClasses_.THUMB);
        /** @type {!Element} */
        var s = document.createElement("span");
        if (s.classList.add(this.CssClasses_.FOCUS_HELPER), div.appendChild(s), this.element_.appendChild(thumb), this.element_.appendChild(div), this.boundMouseUpHandler = this.onMouseUp_.bind(this), this.element_.classList.contains(this.CssClasses_.RIPPLE_EFFECT)) {
          this.element_.classList.add(this.CssClasses_.RIPPLE_IGNORE_EVENTS);
          /** @type {!Element} */
          this.rippleContainerElement_ = document.createElement("span");
          this.rippleContainerElement_.classList.add(this.CssClasses_.RIPPLE_CONTAINER);
          this.rippleContainerElement_.classList.add(this.CssClasses_.RIPPLE_EFFECT);
          this.rippleContainerElement_.classList.add(this.CssClasses_.RIPPLE_CENTER);
          this.rippleContainerElement_.addEventListener("mouseup", this.boundMouseUpHandler);
          /** @type {!Element} */
          var ripple = document.createElement("span");
          ripple.classList.add(this.CssClasses_.RIPPLE);
          this.rippleContainerElement_.appendChild(ripple);
          this.element_.appendChild(this.rippleContainerElement_);
        }
        this.boundChangeHandler = this.onChange_.bind(this);
        this.boundFocusHandler = this.onFocus_.bind(this);
        this.boundBlurHandler = this.onBlur_.bind(this);
        this.inputElement_.addEventListener("change", this.boundChangeHandler);
        this.inputElement_.addEventListener("focus", this.boundFocusHandler);
        this.inputElement_.addEventListener("blur", this.boundBlurHandler);
        this.element_.addEventListener("mouseup", this.boundMouseUpHandler);
        this.updateClasses_();
        this.element_.classList.add("is-upgraded");
      }
    };
    self.register({
      constructor : that,
      classAsString : "MaterialSwitch",
      cssClass : "mdl-js-switch",
      widget : true
    });
    /**
     * @param {!Element} options
     * @return {undefined}
     */
    var Browbeat = function(options) {
      /** @type {!Element} */
      this.element_ = options;
      this.init();
    };
    /** @type {function(!Element): undefined} */
    window.MaterialTabs = Browbeat;
    Browbeat.prototype.Constant_ = {};
    Browbeat.prototype.CssClasses_ = {
      TAB_CLASS : "mdl-tabs__tab",
      PANEL_CLASS : "mdl-tabs__panel",
      ACTIVE_CLASS : "is-active",
      UPGRADED_CLASS : "is-upgraded",
      MDL_JS_RIPPLE_EFFECT : "mdl-js-ripple-effect",
      MDL_RIPPLE_CONTAINER : "mdl-tabs__ripple-container",
      MDL_RIPPLE : "mdl-ripple",
      MDL_JS_RIPPLE_EFFECT_IGNORE_EVENTS : "mdl-js-ripple-effect--ignore-events"
    };
    /**
     * @return {undefined}
     */
    Browbeat.prototype.initTabs_ = function() {
      if (this.element_.classList.contains(this.CssClasses_.MDL_JS_RIPPLE_EFFECT)) {
        this.element_.classList.add(this.CssClasses_.MDL_JS_RIPPLE_EFFECT_IGNORE_EVENTS);
      }
      this.tabs_ = this.element_.querySelectorAll("." + this.CssClasses_.TAB_CLASS);
      this.panels_ = this.element_.querySelectorAll("." + this.CssClasses_.PANEL_CLASS);
      /** @type {number} */
      var i = 0;
      for (; i < this.tabs_.length; i++) {
        new attachClickEvents(this.tabs_[i], this);
      }
      this.element_.classList.add(this.CssClasses_.UPGRADED_CLASS);
    };
    /**
     * @return {undefined}
     */
    Browbeat.prototype.resetTabState_ = function() {
      /** @type {number} */
      var i = 0;
      for (; i < this.tabs_.length; i++) {
        this.tabs_[i].classList.remove(this.CssClasses_.ACTIVE_CLASS);
      }
    };
    /**
     * @return {undefined}
     */
    Browbeat.prototype.resetPanelState_ = function() {
      /** @type {number} */
      var j = 0;
      for (; j < this.panels_.length; j++) {
        this.panels_[j].classList.remove(this.CssClasses_.ACTIVE_CLASS);
      }
    };
    /**
     * @return {undefined}
     */
    Browbeat.prototype.init = function() {
      if (this.element_) {
        this.initTabs_();
      }
    };
    self.register({
      constructor : Browbeat,
      classAsString : "MaterialTabs",
      cssClass : "mdl-js-tabs"
    });
    /**
     * @param {!HTMLElement} element
     * @return {undefined}
     */
    var MaterialTextfield = function(element) {
      /** @type {!HTMLElement} */
      this.element_ = element;
      this.maxRows = this.Constant_.NO_MAX_ROWS;
      this.init();
    };
    /** @type {function(!HTMLElement): undefined} */
    window.MaterialTextfield = MaterialTextfield;
    MaterialTextfield.prototype.Constant_ = {
      NO_MAX_ROWS : -1,
      MAX_ROWS_ATTRIBUTE : "maxrows"
    };
    MaterialTextfield.prototype.CssClasses_ = {
      LABEL : "mdl-textfield__label",
      INPUT : "mdl-textfield__input",
      IS_DIRTY : "is-dirty",
      IS_FOCUSED : "is-focused",
      IS_DISABLED : "is-disabled",
      IS_INVALID : "is-invalid",
      IS_UPGRADED : "is-upgraded",
      HAS_PLACEHOLDER : "has-placeholder"
    };
    /**
     * @param {!Event} event
     * @return {undefined}
     */
    MaterialTextfield.prototype.onKeyDown_ = function(event) {
      var currentRowCount = event.target.value.split("\n").length;
      if (13 === event.keyCode && currentRowCount >= this.maxRows) {
        event.preventDefault();
      }
    };
    /**
     * @param {?} event
     * @return {undefined}
     */
    MaterialTextfield.prototype.onFocus_ = function(event) {
      this.element_.classList.add(this.CssClasses_.IS_FOCUSED);
    };
    /**
     * @param {?} opt_e
     * @return {undefined}
     */
    MaterialTextfield.prototype.onBlur_ = function(opt_e) {
      this.element_.classList.remove(this.CssClasses_.IS_FOCUSED);
    };
    /**
     * @param {?} event
     * @return {undefined}
     */
    MaterialTextfield.prototype.onReset_ = function(event) {
      this.updateClasses_();
    };
    /**
     * @return {undefined}
     */
    MaterialTextfield.prototype.updateClasses_ = function() {
      this.checkDisabled();
      this.checkValidity();
      this.checkDirty();
      this.checkFocus();
    };
    /**
     * @return {undefined}
     */
    MaterialTextfield.prototype.checkDisabled = function() {
      if (this.input_.disabled) {
        this.element_.classList.add(this.CssClasses_.IS_DISABLED);
      } else {
        this.element_.classList.remove(this.CssClasses_.IS_DISABLED);
      }
    };
    /** @type {function(): undefined} */
    MaterialTextfield.prototype.checkDisabled = MaterialTextfield.prototype.checkDisabled;
    /**
     * @return {undefined}
     */
    MaterialTextfield.prototype.checkFocus = function() {
      if (Boolean(this.element_.querySelector(":focus"))) {
        this.element_.classList.add(this.CssClasses_.IS_FOCUSED);
      } else {
        this.element_.classList.remove(this.CssClasses_.IS_FOCUSED);
      }
    };
    /** @type {function(): undefined} */
    MaterialTextfield.prototype.checkFocus = MaterialTextfield.prototype.checkFocus;
    /**
     * @return {undefined}
     */
    MaterialTextfield.prototype.checkValidity = function() {
      if (this.input_.validity) {
        if (this.input_.validity.valid) {
          this.element_.classList.remove(this.CssClasses_.IS_INVALID);
        } else {
          this.element_.classList.add(this.CssClasses_.IS_INVALID);
        }
      }
    };
    /** @type {function(): undefined} */
    MaterialTextfield.prototype.checkValidity = MaterialTextfield.prototype.checkValidity;
    /**
     * @return {undefined}
     */
    MaterialTextfield.prototype.checkDirty = function() {
      if (this.input_.value && this.input_.value.length > 0) {
        this.element_.classList.add(this.CssClasses_.IS_DIRTY);
      } else {
        this.element_.classList.remove(this.CssClasses_.IS_DIRTY);
      }
    };
    /** @type {function(): undefined} */
    MaterialTextfield.prototype.checkDirty = MaterialTextfield.prototype.checkDirty;
    /**
     * @return {undefined}
     */
    MaterialTextfield.prototype.disable = function() {
      /** @type {boolean} */
      this.input_.disabled = true;
      this.updateClasses_();
    };
    /** @type {function(): undefined} */
    MaterialTextfield.prototype.disable = MaterialTextfield.prototype.disable;
    /**
     * @return {undefined}
     */
    MaterialTextfield.prototype.enable = function() {
      /** @type {boolean} */
      this.input_.disabled = false;
      this.updateClasses_();
    };
    /** @type {function(): undefined} */
    MaterialTextfield.prototype.enable = MaterialTextfield.prototype.enable;
    /**
     * @param {string} value
     * @return {undefined}
     */
    MaterialTextfield.prototype.change = function(value) {
      this.input_.value = value || "";
      this.updateClasses_();
    };
    /** @type {function(string): undefined} */
    MaterialTextfield.prototype.change = MaterialTextfield.prototype.change;
    /**
     * @return {undefined}
     */
    MaterialTextfield.prototype.init = function() {
      if (this.element_ && (this.label_ = this.element_.querySelector("." + this.CssClasses_.LABEL), this.input_ = this.element_.querySelector("." + this.CssClasses_.INPUT), this.input_)) {
        if (this.input_.hasAttribute(this.Constant_.MAX_ROWS_ATTRIBUTE)) {
          /** @type {number} */
          this.maxRows = parseInt(this.input_.getAttribute(this.Constant_.MAX_ROWS_ATTRIBUTE), 10);
          if (isNaN(this.maxRows)) {
            this.maxRows = this.Constant_.NO_MAX_ROWS;
          }
        }
        if (this.input_.hasAttribute("placeholder")) {
          this.element_.classList.add(this.CssClasses_.HAS_PLACEHOLDER);
        }
        this.boundUpdateClassesHandler = this.updateClasses_.bind(this);
        this.boundFocusHandler = this.onFocus_.bind(this);
        this.boundBlurHandler = this.onBlur_.bind(this);
        this.boundResetHandler = this.onReset_.bind(this);
        this.input_.addEventListener("input", this.boundUpdateClassesHandler);
        this.input_.addEventListener("focus", this.boundFocusHandler);
        this.input_.addEventListener("blur", this.boundBlurHandler);
        this.input_.addEventListener("reset", this.boundResetHandler);
        if (this.maxRows !== this.Constant_.NO_MAX_ROWS) {
          this.boundKeyDownHandler = this.onKeyDown_.bind(this);
          this.input_.addEventListener("keydown", this.boundKeyDownHandler);
        }
        var e = this.element_.classList.contains(this.CssClasses_.IS_INVALID);
        this.updateClasses_();
        this.element_.classList.add(this.CssClasses_.IS_UPGRADED);
        if (e) {
          this.element_.classList.add(this.CssClasses_.IS_INVALID);
        }
        if (this.input_.hasAttribute("autofocus")) {
          this.element_.focus();
          this.checkFocus();
        }
      }
    };
    self.register({
      constructor : MaterialTextfield,
      classAsString : "MaterialTextfield",
      cssClass : "mdl-js-textfield",
      widget : true
    });
    /**
     * @param {(Document|Element)} element
     * @return {undefined}
     */
    var MaterialKeyvaluelist = function(element) {
      /** @type {(Document|Element)} */
      this.element_ = element;
      this.init();
    };
    /** @type {function((Document|Element)): undefined} */
    window.MaterialTooltip = MaterialKeyvaluelist;
    MaterialKeyvaluelist.prototype.Constant_ = {};
    MaterialKeyvaluelist.prototype.CssClasses_ = {
      IS_ACTIVE : "is-active",
      BOTTOM : "mdl-tooltip--bottom",
      LEFT : "mdl-tooltip--left",
      RIGHT : "mdl-tooltip--right",
      TOP : "mdl-tooltip--top"
    };
    /**
     * @param {!Event} event
     * @return {undefined}
     */
    MaterialKeyvaluelist.prototype.handleMouseEnter_ = function(event) {
      var box = event.target.getBoundingClientRect();
      var targetL = box.left + box.width / 2;
      var y_body_bottom = box.top + box.height / 2;
      /** @type {number} */
      var textboxX = this.element_.offsetWidth / 2 * -1;
      /** @type {number} */
      var value = this.element_.offsetHeight / 2 * -1;
      if (this.element_.classList.contains(this.CssClasses_.LEFT) || this.element_.classList.contains(this.CssClasses_.RIGHT)) {
        /** @type {number} */
        targetL = box.width / 2;
        if (y_body_bottom + value < 0) {
          /** @type {string} */
          this.element_.style.top = "0";
          /** @type {string} */
          this.element_.style.marginTop = "0";
        } else {
          /** @type {string} */
          this.element_.style.top = y_body_bottom + "px";
          /** @type {string} */
          this.element_.style.marginTop = value + "px";
        }
      } else {
        if (targetL + textboxX < 0) {
          /** @type {string} */
          this.element_.style.left = "0";
          /** @type {string} */
          this.element_.style.marginLeft = "0";
        } else {
          /** @type {string} */
          this.element_.style.left = targetL + "px";
          /** @type {string} */
          this.element_.style.marginLeft = textboxX + "px";
        }
      }
      if (this.element_.classList.contains(this.CssClasses_.TOP)) {
        /** @type {string} */
        this.element_.style.top = box.top - this.element_.offsetHeight - 10 + "px";
      } else {
        if (this.element_.classList.contains(this.CssClasses_.RIGHT)) {
          /** @type {string} */
          this.element_.style.left = box.left + box.width + 10 + "px";
        } else {
          if (this.element_.classList.contains(this.CssClasses_.LEFT)) {
            /** @type {string} */
            this.element_.style.left = box.left - this.element_.offsetWidth - 10 + "px";
          } else {
            /** @type {string} */
            this.element_.style.top = box.top + box.height + 10 + "px";
          }
        }
      }
      this.element_.classList.add(this.CssClasses_.IS_ACTIVE);
    };
    /**
     * @return {undefined}
     */
    MaterialKeyvaluelist.prototype.hideTooltip_ = function() {
      this.element_.classList.remove(this.CssClasses_.IS_ACTIVE);
    };
    /**
     * @return {undefined}
     */
    MaterialKeyvaluelist.prototype.init = function() {
      if (this.element_) {
        var forElId = this.element_.getAttribute("for") || this.element_.getAttribute("data-mdl-for");
        if (forElId) {
          /** @type {(Element|null)} */
          this.forElement_ = document.getElementById(forElId);
        }
        if (this.forElement_) {
          if (!this.forElement_.hasAttribute("tabindex")) {
            this.forElement_.setAttribute("tabindex", "0");
          }
          this.boundMouseEnterHandler = this.handleMouseEnter_.bind(this);
          this.boundMouseLeaveAndScrollHandler = this.hideTooltip_.bind(this);
          this.forElement_.addEventListener("mouseenter", this.boundMouseEnterHandler, false);
          this.forElement_.addEventListener("touchend", this.boundMouseEnterHandler, false);
          this.forElement_.addEventListener("mouseleave", this.boundMouseLeaveAndScrollHandler, false);
          window.addEventListener("scroll", this.boundMouseLeaveAndScrollHandler, true);
          window.addEventListener("touchstart", this.boundMouseLeaveAndScrollHandler);
        }
      }
    };
    self.register({
      constructor : MaterialKeyvaluelist,
      classAsString : "MaterialTooltip",
      cssClass : "mdl-tooltip"
    });
    /**
     * @param {!Element} element
     * @return {undefined}
     */
    var MaterialFile = function(element) {
      /** @type {!Element} */
      this.element_ = element;
      this.init();
    };
    /** @type {function(!Element): undefined} */
    window.MaterialLayout = MaterialFile;
    MaterialFile.prototype.Constant_ = {
      MAX_WIDTH : "(max-width: 1024px)",
      TAB_SCROLL_PIXELS : 100,
      RESIZE_TIMEOUT : 100,
      MENU_ICON : "&#xE5D2;",
      CHEVRON_LEFT : "chevron_left",
      CHEVRON_RIGHT : "chevron_right"
    };
    MaterialFile.prototype.Keycodes_ = {
      ENTER : 13,
      ESCAPE : 27,
      SPACE : 32
    };
    MaterialFile.prototype.Mode_ = {
      STANDARD : 0,
      SEAMED : 1,
      WATERFALL : 2,
      SCROLL : 3
    };
    MaterialFile.prototype.CssClasses_ = {
      CONTAINER : "mdl-layout__container",
      HEADER : "mdl-layout__header",
      DRAWER : "mdl-layout__drawer",
      CONTENT : "mdl-layout__content",
      DRAWER_BTN : "mdl-layout__drawer-button",
      ICON : "material-icons",
      JS_RIPPLE_EFFECT : "mdl-js-ripple-effect",
      RIPPLE_CONTAINER : "mdl-layout__tab-ripple-container",
      RIPPLE : "mdl-ripple",
      RIPPLE_IGNORE_EVENTS : "mdl-js-ripple-effect--ignore-events",
      HEADER_SEAMED : "mdl-layout__header--seamed",
      HEADER_WATERFALL : "mdl-layout__header--waterfall",
      HEADER_SCROLL : "mdl-layout__header--scroll",
      FIXED_HEADER : "mdl-layout--fixed-header",
      OBFUSCATOR : "mdl-layout__obfuscator",
      TAB_BAR : "mdl-layout__tab-bar",
      TAB_CONTAINER : "mdl-layout__tab-bar-container",
      TAB : "mdl-layout__tab",
      TAB_BAR_BUTTON : "mdl-layout__tab-bar-button",
      TAB_BAR_LEFT_BUTTON : "mdl-layout__tab-bar-left-button",
      TAB_BAR_RIGHT_BUTTON : "mdl-layout__tab-bar-right-button",
      TAB_MANUAL_SWITCH : "mdl-layout__tab-manual-switch",
      PANEL : "mdl-layout__tab-panel",
      HAS_DRAWER : "has-drawer",
      HAS_TABS : "has-tabs",
      HAS_SCROLLING_HEADER : "has-scrolling-header",
      CASTING_SHADOW : "is-casting-shadow",
      IS_COMPACT : "is-compact",
      IS_SMALL_SCREEN : "is-small-screen",
      IS_DRAWER_OPEN : "is-visible",
      IS_ACTIVE : "is-active",
      IS_UPGRADED : "is-upgraded",
      IS_ANIMATING : "is-animating",
      ON_LARGE_SCREEN : "mdl-layout--large-screen-only",
      ON_SMALL_SCREEN : "mdl-layout--small-screen-only"
    };
    /**
     * @return {undefined}
     */
    MaterialFile.prototype.contentScrollHandler_ = function() {
      if (!this.header_.classList.contains(this.CssClasses_.IS_ANIMATING)) {
        var e = !this.element_.classList.contains(this.CssClasses_.IS_SMALL_SCREEN) || this.element_.classList.contains(this.CssClasses_.FIXED_HEADER);
        if (this.content_.scrollTop > 0 && !this.header_.classList.contains(this.CssClasses_.IS_COMPACT)) {
          this.header_.classList.add(this.CssClasses_.CASTING_SHADOW);
          this.header_.classList.add(this.CssClasses_.IS_COMPACT);
          if (e) {
            this.header_.classList.add(this.CssClasses_.IS_ANIMATING);
          }
        } else {
          if (this.content_.scrollTop <= 0 && this.header_.classList.contains(this.CssClasses_.IS_COMPACT)) {
            this.header_.classList.remove(this.CssClasses_.CASTING_SHADOW);
            this.header_.classList.remove(this.CssClasses_.IS_COMPACT);
            if (e) {
              this.header_.classList.add(this.CssClasses_.IS_ANIMATING);
            }
          }
        }
      }
    };
    /**
     * @param {!Event} evt
     * @return {undefined}
     */
    MaterialFile.prototype.keyboardEventHandler_ = function(evt) {
      if (evt.keyCode === this.Keycodes_.ESCAPE && this.drawer_.classList.contains(this.CssClasses_.IS_DRAWER_OPEN)) {
        this.toggleDrawer();
      }
    };
    /**
     * @return {undefined}
     */
    MaterialFile.prototype.screenSizeHandler_ = function() {
      if (this.screenSizeMediaQuery_.matches) {
        this.element_.classList.add(this.CssClasses_.IS_SMALL_SCREEN);
      } else {
        this.element_.classList.remove(this.CssClasses_.IS_SMALL_SCREEN);
        if (this.drawer_) {
          this.drawer_.classList.remove(this.CssClasses_.IS_DRAWER_OPEN);
          this.obfuscator_.classList.remove(this.CssClasses_.IS_DRAWER_OPEN);
        }
      }
    };
    /**
     * @param {!Object} e
     * @return {undefined}
     */
    MaterialFile.prototype.drawerToggleHandler_ = function(e) {
      if (e && "keydown" === e.type) {
        if (e.keyCode !== this.Keycodes_.SPACE && e.keyCode !== this.Keycodes_.ENTER) {
          return;
        }
        e.preventDefault();
      }
      this.toggleDrawer();
    };
    /**
     * @return {undefined}
     */
    MaterialFile.prototype.headerTransitionEndHandler_ = function() {
      this.header_.classList.remove(this.CssClasses_.IS_ANIMATING);
    };
    /**
     * @return {undefined}
     */
    MaterialFile.prototype.headerClickHandler_ = function() {
      if (this.header_.classList.contains(this.CssClasses_.IS_COMPACT)) {
        this.header_.classList.remove(this.CssClasses_.IS_COMPACT);
        this.header_.classList.add(this.CssClasses_.IS_ANIMATING);
      }
    };
    /**
     * @param {!NodeList} tabBar
     * @return {undefined}
     */
    MaterialFile.prototype.resetTabState_ = function(tabBar) {
      /** @type {number} */
      var k = 0;
      for (; k < tabBar.length; k++) {
        tabBar[k].classList.remove(this.CssClasses_.IS_ACTIVE);
      }
    };
    /**
     * @param {!NodeList} panels
     * @return {undefined}
     */
    MaterialFile.prototype.resetPanelState_ = function(panels) {
      /** @type {number} */
      var i = 0;
      for (; i < panels.length; i++) {
        panels[i].classList.remove(this.CssClasses_.IS_ACTIVE);
      }
    };
    /**
     * @return {undefined}
     */
    MaterialFile.prototype.toggleDrawer = function() {
      var headerGroup = this.element_.querySelector("." + this.CssClasses_.DRAWER_BTN);
      this.drawer_.classList.toggle(this.CssClasses_.IS_DRAWER_OPEN);
      this.obfuscator_.classList.toggle(this.CssClasses_.IS_DRAWER_OPEN);
      if (this.drawer_.classList.contains(this.CssClasses_.IS_DRAWER_OPEN)) {
        this.drawer_.setAttribute("aria-hidden", "false");
        headerGroup.setAttribute("aria-expanded", "true");
      } else {
        this.drawer_.setAttribute("aria-hidden", "true");
        headerGroup.setAttribute("aria-expanded", "false");
      }
    };
    /** @type {function(): undefined} */
    MaterialFile.prototype.toggleDrawer = MaterialFile.prototype.toggleDrawer;
    /**
     * @return {undefined}
     */
    MaterialFile.prototype.init = function() {
      if (this.element_) {
        /** @type {!Element} */
        var container = document.createElement("div");
        container.classList.add(this.CssClasses_.CONTAINER);
        var inlineEditor2 = this.element_.querySelector(":focus");
        this.element_.parentElement.insertBefore(container, this.element_);
        this.element_.parentElement.removeChild(this.element_);
        container.appendChild(this.element_);
        if (inlineEditor2) {
          inlineEditor2.focus();
        }
        var dom = this.element_.childNodes;
        var rows = dom.length;
        /** @type {number} */
        var i = 0;
        for (; i < rows; i++) {
          var child = dom[i];
          if (child.classList && child.classList.contains(this.CssClasses_.HEADER)) {
            this.header_ = child;
          }
          if (child.classList && child.classList.contains(this.CssClasses_.DRAWER)) {
            this.drawer_ = child;
          }
          if (child.classList && child.classList.contains(this.CssClasses_.CONTENT)) {
            this.content_ = child;
          }
        }
        window.addEventListener("pageshow", function(state) {
          if (state.persisted) {
            /** @type {string} */
            this.element_.style.overflowY = "hidden";
            requestAnimationFrame(function() {
              /** @type {string} */
              this.element_.style.overflowY = "";
            }.bind(this));
          }
        }.bind(this), false);
        if (this.header_) {
          this.tabBar_ = this.header_.querySelector("." + this.CssClasses_.TAB_BAR);
        }
        var mode = this.Mode_.STANDARD;
        if (this.header_ && (this.header_.classList.contains(this.CssClasses_.HEADER_SEAMED) ? mode = this.Mode_.SEAMED : this.header_.classList.contains(this.CssClasses_.HEADER_WATERFALL) ? (mode = this.Mode_.WATERFALL, this.header_.addEventListener("transitionend", this.headerTransitionEndHandler_.bind(this)), this.header_.addEventListener("click", this.headerClickHandler_.bind(this))) : this.header_.classList.contains(this.CssClasses_.HEADER_SCROLL) && (mode = this.Mode_.SCROLL, container.classList.add(this.CssClasses_.HAS_SCROLLING_HEADER)), 
        mode === this.Mode_.STANDARD ? (this.header_.classList.add(this.CssClasses_.CASTING_SHADOW), this.tabBar_ && this.tabBar_.classList.add(this.CssClasses_.CASTING_SHADOW)) : mode === this.Mode_.SEAMED || mode === this.Mode_.SCROLL ? (this.header_.classList.remove(this.CssClasses_.CASTING_SHADOW), this.tabBar_ && this.tabBar_.classList.remove(this.CssClasses_.CASTING_SHADOW)) : mode === this.Mode_.WATERFALL && (this.content_.addEventListener("scroll", this.contentScrollHandler_.bind(this)), 
        this.contentScrollHandler_())), this.drawer_) {
          var element = this.element_.querySelector("." + this.CssClasses_.DRAWER_BTN);
          if (!element) {
            (element = document.createElement("div")).setAttribute("aria-expanded", "false");
            element.setAttribute("role", "button");
            element.setAttribute("tabindex", "0");
            element.classList.add(this.CssClasses_.DRAWER_BTN);
            /** @type {!Element} */
            var d = document.createElement("i");
            d.classList.add(this.CssClasses_.ICON);
            d.innerHTML = this.Constant_.MENU_ICON;
            element.appendChild(d);
          }
          if (this.drawer_.classList.contains(this.CssClasses_.ON_LARGE_SCREEN)) {
            element.classList.add(this.CssClasses_.ON_LARGE_SCREEN);
          } else {
            if (this.drawer_.classList.contains(this.CssClasses_.ON_SMALL_SCREEN)) {
              element.classList.add(this.CssClasses_.ON_SMALL_SCREEN);
            }
          }
          element.addEventListener("click", this.drawerToggleHandler_.bind(this));
          element.addEventListener("keydown", this.drawerToggleHandler_.bind(this));
          this.element_.classList.add(this.CssClasses_.HAS_DRAWER);
          if (this.element_.classList.contains(this.CssClasses_.FIXED_HEADER)) {
            this.header_.insertBefore(element, this.header_.firstChild);
          } else {
            this.element_.insertBefore(element, this.content_);
          }
          /** @type {!Element} */
          var obfuscator = document.createElement("div");
          obfuscator.classList.add(this.CssClasses_.OBFUSCATOR);
          this.element_.appendChild(obfuscator);
          obfuscator.addEventListener("click", this.drawerToggleHandler_.bind(this));
          /** @type {!Element} */
          this.obfuscator_ = obfuscator;
          this.drawer_.addEventListener("keydown", this.keyboardEventHandler_.bind(this));
          this.drawer_.setAttribute("aria-hidden", "true");
        }
        if (this.screenSizeMediaQuery_ = window.matchMedia(this.Constant_.MAX_WIDTH), this.screenSizeMediaQuery_.addListener(this.screenSizeHandler_.bind(this)), this.screenSizeHandler_(), this.header_ && this.tabBar_) {
          this.element_.classList.add(this.CssClasses_.HAS_TABS);
          /** @type {!Element} */
          var tabContainer = document.createElement("div");
          tabContainer.classList.add(this.CssClasses_.TAB_CONTAINER);
          this.header_.insertBefore(tabContainer, this.tabBar_);
          this.header_.removeChild(this.tabBar_);
          /** @type {!Element} */
          var leftButton = document.createElement("div");
          leftButton.classList.add(this.CssClasses_.TAB_BAR_BUTTON);
          leftButton.classList.add(this.CssClasses_.TAB_BAR_LEFT_BUTTON);
          /** @type {!Element} */
          var leftButtonIcon = document.createElement("i");
          leftButtonIcon.classList.add(this.CssClasses_.ICON);
          leftButtonIcon.textContent = this.Constant_.CHEVRON_LEFT;
          leftButton.appendChild(leftButtonIcon);
          leftButton.addEventListener("click", function() {
            this.tabBar_.scrollLeft -= this.Constant_.TAB_SCROLL_PIXELS;
          }.bind(this));
          /** @type {!Element} */
          var rightButton = document.createElement("div");
          rightButton.classList.add(this.CssClasses_.TAB_BAR_BUTTON);
          rightButton.classList.add(this.CssClasses_.TAB_BAR_RIGHT_BUTTON);
          /** @type {!Element} */
          var rightButtonIcon = document.createElement("i");
          rightButtonIcon.classList.add(this.CssClasses_.ICON);
          rightButtonIcon.textContent = this.Constant_.CHEVRON_RIGHT;
          rightButton.appendChild(rightButtonIcon);
          rightButton.addEventListener("click", function() {
            this.tabBar_.scrollLeft += this.Constant_.TAB_SCROLL_PIXELS;
          }.bind(this));
          tabContainer.appendChild(leftButton);
          tabContainer.appendChild(this.tabBar_);
          tabContainer.appendChild(rightButton);
          var tabScrollHandler = function() {
            if (this.tabBar_.scrollLeft > 0) {
              leftButton.classList.add(this.CssClasses_.IS_ACTIVE);
            } else {
              leftButton.classList.remove(this.CssClasses_.IS_ACTIVE);
            }
            if (this.tabBar_.scrollLeft < this.tabBar_.scrollWidth - this.tabBar_.offsetWidth) {
              rightButton.classList.add(this.CssClasses_.IS_ACTIVE);
            } else {
              rightButton.classList.remove(this.CssClasses_.IS_ACTIVE);
            }
          }.bind(this);
          this.tabBar_.addEventListener("scroll", tabScrollHandler);
          tabScrollHandler();
          var onInspectorMove = function() {
            if (this.resizeTimeoutId_) {
              clearTimeout(this.resizeTimeoutId_);
            }
            /** @type {number} */
            this.resizeTimeoutId_ = setTimeout(function() {
              tabScrollHandler();
              /** @type {null} */
              this.resizeTimeoutId_ = null;
            }.bind(this), this.Constant_.RESIZE_TIMEOUT);
          }.bind(this);
          window.addEventListener("resize", onInspectorMove);
          if (this.tabBar_.classList.contains(this.CssClasses_.JS_RIPPLE_EFFECT)) {
            this.tabBar_.classList.add(this.CssClasses_.RIPPLE_IGNORE_EVENTS);
          }
          var crossfilterable_layers = this.tabBar_.querySelectorAll("." + this.CssClasses_.TAB);
          var raw = this.content_.querySelectorAll("." + this.CssClasses_.PANEL);
          /** @type {number} */
          var layer_i = 0;
          for (; layer_i < crossfilterable_layers.length; layer_i++) {
            new t(crossfilterable_layers[layer_i], crossfilterable_layers, raw, this);
          }
        }
        this.element_.classList.add(this.CssClasses_.IS_UPGRADED);
      }
    };
    /** @type {function(!Object, (Node|NodeList|string), (Node|NodeList|string), ?): undefined} */
    window.MaterialLayoutTab = t;
    self.register({
      constructor : MaterialFile,
      classAsString : "MaterialLayout",
      cssClass : "mdl-js-layout"
    });
    /**
     * @param {!Element} element
     * @return {undefined}
     */
    var DragDropController = function(element) {
      /** @type {!Element} */
      this.element_ = element;
      this.init();
    };
    /** @type {function(!Element): undefined} */
    window.MaterialDataTable = DragDropController;
    DragDropController.prototype.Constant_ = {};
    DragDropController.prototype.CssClasses_ = {
      DATA_TABLE : "mdl-data-table",
      SELECTABLE : "mdl-data-table--selectable",
      SELECT_ELEMENT : "mdl-data-table__select",
      IS_SELECTED : "is-selected",
      IS_UPGRADED : "is-upgraded"
    };
    /**
     * @param {!Element} checkbox
     * @param {!Object} row
     * @param {string} rows
     * @return {?}
     */
    DragDropController.prototype.selectRow_ = function(checkbox, row, rows) {
      return row ? function() {
        if (checkbox.checked) {
          row.classList.add(this.CssClasses_.IS_SELECTED);
        } else {
          row.classList.remove(this.CssClasses_.IS_SELECTED);
        }
      }.bind(this) : rows ? function() {
        var i;
        if (checkbox.checked) {
          /** @type {number} */
          i = 0;
          for (; i < rows.length; i++) {
            rows[i].querySelector("td").querySelector(".mdl-checkbox").MaterialCheckbox.check();
            rows[i].classList.add(this.CssClasses_.IS_SELECTED);
          }
        } else {
          /** @type {number} */
          i = 0;
          for (; i < rows.length; i++) {
            rows[i].querySelector("td").querySelector(".mdl-checkbox").MaterialCheckbox.uncheck();
            rows[i].classList.remove(this.CssClasses_.IS_SELECTED);
          }
        }
      }.bind(this) : void 0;
    };
    /**
     * @param {!Object} row
     * @param {!Function} rows
     * @return {?}
     */
    DragDropController.prototype.createCheckbox_ = function(row, rows) {
      /** @type {!Element} */
      var label = document.createElement("label");
      /** @type {!Array} */
      var MDL_CLASSES = ["mdl-checkbox", "mdl-js-checkbox", "mdl-js-ripple-effect", this.CssClasses_.SELECT_ELEMENT];
      /** @type {string} */
      label.className = MDL_CLASSES.join(" ");
      /** @type {!Element} */
      var checkbox = document.createElement("input");
      return checkbox.type = "checkbox", checkbox.classList.add("mdl-checkbox__input"), row ? (checkbox.checked = row.classList.contains(this.CssClasses_.IS_SELECTED), checkbox.addEventListener("change", this.selectRow_(checkbox, row))) : rows && checkbox.addEventListener("change", this.selectRow_(checkbox, null, rows)), label.appendChild(checkbox), self.upgradeElement(label, "MaterialCheckbox"), label;
    };
    /**
     * @return {undefined}
     */
    DragDropController.prototype.init = function() {
      if (this.element_) {
        var firstHeader = this.element_.querySelector("th");
        /** @type {!Array<?>} */
        var addSet = Array.prototype.slice.call(this.element_.querySelectorAll("tbody tr"));
        /** @type {!Array<?>} */
        var values = Array.prototype.slice.call(this.element_.querySelectorAll("tfoot tr"));
        /** @type {!Array<?>} */
        var rows = addSet.concat(values);
        if (this.element_.classList.contains(this.CssClasses_.SELECTABLE)) {
          /** @type {!Element} */
          var th = document.createElement("th");
          var headerCheckbox = this.createCheckbox_(null, rows);
          th.appendChild(headerCheckbox);
          firstHeader.parentElement.insertBefore(th, firstHeader);
          /** @type {number} */
          var i = 0;
          for (; i < rows.length; i++) {
            var originalBluePrintNode = rows[i].querySelector("td");
            if (originalBluePrintNode) {
              /** @type {!Element} */
              var td = document.createElement("td");
              if ("TBODY" === rows[i].parentNode.nodeName.toUpperCase()) {
                var rowCheckbox = this.createCheckbox_(rows[i]);
                td.appendChild(rowCheckbox);
              }
              rows[i].insertBefore(td, originalBluePrintNode);
            }
          }
          this.element_.classList.add(this.CssClasses_.IS_UPGRADED);
        }
      }
    };
    self.register({
      constructor : DragDropController,
      classAsString : "MaterialDataTable",
      cssClass : "mdl-js-data-table"
    });
    /**
     * @param {!HTMLElement} elem
     * @return {undefined}
     */
    var DonkeyJump = function(elem) {
      /** @type {!HTMLElement} */
      this.element_ = elem;
      this.init();
    };
    /** @type {function(!HTMLElement): undefined} */
    window.MaterialRipple = DonkeyJump;
    DonkeyJump.prototype.Constant_ = {
      INITIAL_SCALE : "scale(0.0001, 0.0001)",
      INITIAL_SIZE : "1px",
      INITIAL_OPACITY : "0.4",
      FINAL_OPACITY : "0",
      FINAL_SCALE : ""
    };
    DonkeyJump.prototype.CssClasses_ = {
      RIPPLE_CENTER : "mdl-ripple--center",
      RIPPLE_EFFECT_IGNORE_EVENTS : "mdl-js-ripple-effect--ignore-events",
      RIPPLE : "mdl-ripple",
      IS_ANIMATING : "is-animating",
      IS_VISIBLE : "is-visible"
    };
    /**
     * @param {!Event} e
     * @return {undefined}
     */
    DonkeyJump.prototype.downHandler_ = function(e) {
      if (!this.rippleElement_.style.width && !this.rippleElement_.style.height) {
        var rect = this.element_.getBoundingClientRect();
        this.boundHeight = rect.height;
        this.boundWidth = rect.width;
        /** @type {number} */
        this.rippleSize_ = 2 * Math.sqrt(rect.width * rect.width + rect.height * rect.height) + 2;
        /** @type {string} */
        this.rippleElement_.style.width = this.rippleSize_ + "px";
        /** @type {string} */
        this.rippleElement_.style.height = this.rippleSize_ + "px";
      }
      if (this.rippleElement_.classList.add(this.CssClasses_.IS_VISIBLE), "mousedown" === e.type && this.ignoringMouseDown_) {
        /** @type {boolean} */
        this.ignoringMouseDown_ = false;
      } else {
        if ("touchstart" === e.type && (this.ignoringMouseDown_ = true), this.getFrameCount() > 0) {
          return;
        }
        this.setFrameCount(1);
        var x;
        var y;
        var self = e.currentTarget.getBoundingClientRect();
        if (0 === e.clientX && 0 === e.clientY) {
          /** @type {number} */
          x = Math.round(self.width / 2);
          /** @type {number} */
          y = Math.round(self.height / 2);
        } else {
          var now = void 0 !== e.clientX ? e.clientX : e.touches[0].clientX;
          var pageY = void 0 !== e.clientY ? e.clientY : e.touches[0].clientY;
          /** @type {number} */
          x = Math.round(now - self.left);
          /** @type {number} */
          y = Math.round(pageY - self.top);
        }
        this.setRippleXY(x, y);
        this.setRippleStyles(true);
        window.requestAnimationFrame(this.animFrameHandler.bind(this));
      }
    };
    /**
     * @param {!Object} event
     * @return {undefined}
     */
    DonkeyJump.prototype.upHandler_ = function(event) {
      if (event && 2 !== event.detail) {
        window.setTimeout(function() {
          this.rippleElement_.classList.remove(this.CssClasses_.IS_VISIBLE);
        }.bind(this), 0);
      }
    };
    /**
     * @return {undefined}
     */
    DonkeyJump.prototype.init = function() {
      if (this.element_) {
        var e = this.element_.classList.contains(this.CssClasses_.RIPPLE_CENTER);
        if (!this.element_.classList.contains(this.CssClasses_.RIPPLE_EFFECT_IGNORE_EVENTS)) {
          this.rippleElement_ = this.element_.querySelector("." + this.CssClasses_.RIPPLE);
          /** @type {number} */
          this.frameCount_ = 0;
          /** @type {number} */
          this.rippleSize_ = 0;
          /** @type {number} */
          this.x_ = 0;
          /** @type {number} */
          this.y_ = 0;
          /** @type {boolean} */
          this.ignoringMouseDown_ = false;
          this.boundDownHandler = this.downHandler_.bind(this);
          this.element_.addEventListener("mousedown", this.boundDownHandler);
          this.element_.addEventListener("touchstart", this.boundDownHandler);
          this.boundUpHandler = this.upHandler_.bind(this);
          this.element_.addEventListener("mouseup", this.boundUpHandler);
          this.element_.addEventListener("mouseleave", this.boundUpHandler);
          this.element_.addEventListener("touchend", this.boundUpHandler);
          this.element_.addEventListener("blur", this.boundUpHandler);
          /**
           * @return {?}
           */
          this.getFrameCount = function() {
            return this.frameCount_;
          };
          /**
           * @param {number} fC
           * @return {undefined}
           */
          this.setFrameCount = function(fC) {
            /** @type {number} */
            this.frameCount_ = fC;
          };
          /**
           * @return {?}
           */
          this.getRippleElement = function() {
            return this.rippleElement_;
          };
          /**
           * @param {number} newX
           * @param {number} newY
           * @return {undefined}
           */
          this.setRippleXY = function(newX, newY) {
            /** @type {number} */
            this.x_ = newX;
            /** @type {number} */
            this.y_ = newY;
          };
          /**
           * @param {boolean} start
           * @return {undefined}
           */
          this.setRippleStyles = function(start) {
            if (null !== this.rippleElement_) {
              var transformString;
              var scale;
              /** @type {string} */
              var skew = "translate(" + this.x_ + "px, " + this.y_ + "px)";
              if (start) {
                scale = this.Constant_.INITIAL_SCALE;
                this.Constant_.INITIAL_SIZE;
              } else {
                scale = this.Constant_.FINAL_SCALE;
                this.rippleSize_ + "px";
                if (e) {
                  /** @type {string} */
                  skew = "translate(" + this.boundWidth / 2 + "px, " + this.boundHeight / 2 + "px)";
                }
              }
              /** @type {string} */
              transformString = "translate(-50%, -50%) " + skew + scale;
              /** @type {string} */
              this.rippleElement_.style.webkitTransform = transformString;
              /** @type {string} */
              this.rippleElement_.style.msTransform = transformString;
              /** @type {string} */
              this.rippleElement_.style.transform = transformString;
              if (start) {
                this.rippleElement_.classList.remove(this.CssClasses_.IS_ANIMATING);
              } else {
                this.rippleElement_.classList.add(this.CssClasses_.IS_ANIMATING);
              }
            }
          };
          /**
           * @return {undefined}
           */
          this.animFrameHandler = function() {
            if (this.frameCount_-- > 0) {
              window.requestAnimationFrame(this.animFrameHandler.bind(this));
            } else {
              this.setRippleStyles(false);
            }
          };
        }
      }
    };
    self.register({
      constructor : DonkeyJump,
      classAsString : "MaterialRipple",
      cssClass : "mdl-js-ripple-effect",
      widget : false
    });
  }();
}]);
